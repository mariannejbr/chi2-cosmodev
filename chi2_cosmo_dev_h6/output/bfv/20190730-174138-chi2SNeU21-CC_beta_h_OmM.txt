# 
# Union 2.1
# model: beta free to vary within 10^-10 and 1
# h and Omega_m fixed to Planck cosmology
# 
# bounds in the order (h, om_m, beta):
# 
#  = [(0, 1), (0, 1), (0, 1e-10)]
# 
# Results are displayed in the following order: 
# chi2min, chi2reduced,
# beta, h, OmegaM, aic, bic
# 
5.597158953024518269e+02
9.617111603134911313e-01
1.000000000000000036e-10
7.000849996001556885e-01
2.679085625350427491e-01
5.788307308441474106e+02
5.657158953024518269e+02
