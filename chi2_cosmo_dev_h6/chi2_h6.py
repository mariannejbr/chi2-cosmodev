from functools import lru_cache
from math import pi, sqrt
from multiprocessing.pool import Pool
from progressbar import Bar, ETA, Percentage, ProgressBar
from .cosmology_for_h6 import  mu_SNe, hubble_6
from Input.hz_data import DATA_Hz, REDSHIFTS_Hz, ERROR_Hz
from Input.SNeIa_Union2_1 import DATA_SNe, REDSHIFTS_SNe, ERROR_SNe
from Constants_Units import *


# ===============        For SNeIa          =================
# ===============      binned data          =================


def chi2SNe(model_params, cosmo_params):
    '''chi2 function for SNe JLA data (uncorrelated data)'''
    chi_sum = 0
    for (zi, obs, err) in DATA_SNe:
        # Errors are already squared both for JLA and Union 2.1 SNe datasets
        chi_sum += ((obs - mu_SNe(zi, model_params, cosmo_params))) ** 2 / err
    return chi_sum


@lru_cache(maxsize=1024 * 1024, typed=True)
def LikelihoodSNe(model_params, cosmo_params):
    """Likelihood associated to the binned SNe data
    # todo: mariana add definition
    """
    m = len(REDSHIFTS_SNe)
    return (
            np.exp(-chi2SNe(model_params, cosmo_params) / 2) / ((2 * pi) ** (
            m / 2) * (1 / np.prod(ERROR_SNe)) ** (1 / 2))
        # todo: check the exponent in np.prod(Error) factor

    )


def loglikeSNe(model_params, cosmo_params):
    return -chi2SNe(model_params, cosmo_params) / 2


# ===============        For H(z)                =================
# ===============    from cosmic clocks          =================

def chi2hz(model_params, cosmo_params):
    ''' chi2 function for H(z) data from cosmic clocks '''
    chi_sum = 0
    for (zi, obs, err) in DATA_Hz:
        chi_sum += ((obs - hubble_6(zi, model_params,
                                            cosmo_params)) / err) ** 2

    return chi_sum


def loglikehz(model_params, cosmo_params):
    return - chi2hz(model_params, cosmo_params) / 2


def likehz(model_params, cosmo_params):
    '''
    likelihood for the values of H(z) from cosmic clocks data
    :param model_params:  w0, w1, w2, w3
    :param cosmo_params:  omegam, h0
    :return:
    '''
    m = len(REDSHIFTS_Hz)
    return (
            np.exp(-chi2hz(model_params, cosmo_params) / 2) / ((2 * pi) ** (
            m / 2) * (1 / np.prod(ERROR_Hz)) ** (1 / 2))

    )


# =============== Combining datasets: chi2 & likelihood =================
# ===============           BAO + SNe                   =================


@lru_cache(maxsize=1024 * 1024, typed=True)
def chi2CCSNe(model_params, cosmo_params):
    """sum of BAO + H0 chi2 functions"""

    #h, OmegaM, mu0 = cosmo_params
    h, OmegaM = cosmo_params
    cosmoparams = h, OmegaM

    return (
            chi2hz(model_params, cosmoparams) +
            chi2SNe(model_params, cosmo_params)
    )


def LikeliCCSNe(model_params, cosmo_params):
    """product of all the likelihood"""

    h, OmegaM, mu0 = cosmo_params
    cosmoparams = h, OmegaM

    return (
            LikelihoodSNe(model_params, cosmo_params) *
            # LikeBAOcorr(w0, w1, w2, h, omegach2) *
            likehz(model_params, cosmoparams)
    )


# parallel implementation of chi2 function either CMB, BAO or BAO-CMB

def parallel_chi2any(kernel, data_array, processes=None):
    """Evaluates the chi2BAO function over the data saved in `data_array`
    and distributes the workload among several independent python
    processes.
    """

    # Let's create a pool of processes to execute calculate chi2BAO in
    # parallel.

    assert hasattr(data_array, 'dtype')

    data_values = data_array.shape[0]

    percent = Percentage()
    eta = ETA()
    bar = Bar(marker='■')
    progress_bar = ProgressBar(max_value=data_values,
                               widgets=[
                                   '[', percent, ']',
                                   bar,
                                   '(', eta, ')'
                               ])

    with Pool(processes=processes) as pool:
        # The data accepted by the map method must be an iterable, like
        # a list, a tuple or a numpy array. The function is applied over
        # each element of the iterable. The result is another list with
        # the values returned by the function after being evaluated.
        #
        # [a, b, c, ...]  -> [f(a), f(b), f(c), ...]
        #
        # Here we use the imap method, so we need to create a list to
        # gather the results.
        results = []
        pool_imap = pool.imap(kernel, data_array)
        progress = 0
        for result in pool_imap:
            results.append(result)
            progress += 1
            progress_bar.update(progress)

    return np.array(results)

# pp module parallel python: 1306.0573
