# # # # # # # # CUBIC COSMOLOGY  # # # # # # # # # # # # # # # # # # # # #
# # # Code developers: Mariana Jaber  & Omar A. Rodríguez-López
# # # Theory developed by Luisa Jaime and presented in 
# # # arXiv:1810.08166 ( CUBIC COSMOLOGY )
# # # This part of the program contains the basic cosmological parameters
# # # such as energy and physical densities for a Flat LCDM
# # # cosmology. It is based on Planck 2015 Cosmological Parameters
# # # report (arxiv:1502.01589v2) table 3 column 4:
# # # "Planck + TT + TE + EE + low P".
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

from math import pi
from functools import lru_cache
from math import sqrt
from numba import carray, cfunc, jit, types
from scipy import LowLevelCallable, integrate
from Constants_Units import *

# ==========   Numerical integration quantities for the calculation ===========
QUAD_EPSABS = 1.49e-12
inf = np.inf
quad = integrate.quad

# ------############ HUBBLE FUNCTION ################-----------#

# # # #  # # # New basic definition of the Hubble function as the root
# of a  polynomial of order sixth in H(z)


def const_coeff(z, beta, cosmo_params):
    h, Omega_m = cosmo_params
    Gnew = 6.67e-11
    c_speed = C
    kappa = 8*np.pi*Gnew/c_speed**4
    om_r = OMEGAR0
    om_l = 1-Omega_m-om_r
    return -(100*h)**2*(1+16*beta*kappa*(100*h)**4)*(Omega_m*(1+z)**3+om_r*(1+z)**4+om_l)


def sixth_coeff(beta):
    Gnew = 6.67e-11
    c_speed = C
    kappa = 8*np.pi*Gnew/c_speed**4
    return 16 * kappa * beta


@lru_cache(maxsize=1024**2)
def hubble_6(z, model_params, cosmo_params):
    """

    """
    h, om_m = cosmo_params
    beta, = model_params

    coeff6 = sixth_coeff(beta)
    coeff0 = const_coeff(z, beta, cosmo_params)

    poly_coeffs = (coeff6, 0, 0, 0, 1, 0, coeff0)
    hubble_poly_roots = np.roots(poly_coeffs)

    roots_cond = np.logical_and(
        np.absolute(hubble_poly_roots.imag) <= 1e-20, hubble_poly_roots.real >= 0)
#     hubble_poly_roots[roots_cond].real >= 0

    return hubble_poly_roots[roots_cond].real[0]




# # # ------- Cosmic distances derived from the Hubble function-------####

# ===============        For BAO scale          =================
# ===============             BAO               =================

@jit(nopython=True)
def btog(z, h):
    """Baryon-to-photon ratio as function of z"""
    # return 3 * OMEGAB0 / (4 * OMEGAG0 * (1 + z))
    return 3 * (OMEGABH2 / h ** 2) / (4 * OMEGAG0 * (1 + z))


@lru_cache(maxsize=1024 * 1024)
def r_s_integrand(z, model_params, cosmo_params):
    ''' flat universe '''

    h, OmegaM = cosmo_params
    cs = 1 / sqrt(3 * (1 + btog(z, h)))

    return cs / hubble_6(z, model_params, cosmo_params)


@lru_cache(maxsize=1024 * 1024)
def r_s(zeval, model_params, cosmo_params, exp_uplim=None):
    """Sound horizon at zeval either zdrag or zdec"""
    exp_uplim = exp_uplim or inf
    r_sound, error = integrate.quad(r_s_integrand, zeval, exp_uplim,
                                    epsabs=QUAD_EPSABS,
                                    args=(model_params, cosmo_params)
                                    )
    return r_sound


def d_ang_integrand(zp, model_params, cosmo_params):
    """Integrand for the angular diameter distance: dz/H(z)"""
    """hubblefunc = H0p * h * sqrt(
        OMEGAR0 * (1 + z) ** 4 + OmegaM * (1 + z) ** 3 +
        (1 - OMEGAR0 - OmegaM) * f_DEz(z, model_params))"""

    return 1 / hubble_6(zp, model_params, cosmo_params)


def d_ang(z, model_params, cosmo_params):
    """Angular Diameter distance:
    D_a(z) = c/(1+z)Int_0^z(dz'/H(z'))
    """
    int, error = integrate.quad(d_ang_integrand, 0, z,
                                epsabs=QUAD_EPSABS,
                                args=(model_params, cosmo_params))
    return 1 / (1 + z) * int


def Rcmb(model_params, cosmo_params):
    '''
    the distance we need is the comoving distance to the LSS
    i.e. D_ang(z) * (1+z)
    '''
    h, OmegaM = cosmo_params
    factor1 = np.sqrt(OmegaM) * h * H0p
    Dangzstar = d_ang(ZDEC, model_params, cosmo_params) * (1 + ZDEC)
    return factor1 * Dangzstar


@lru_cache(maxsize=1024 * 1024)
def d_vz_integrand(zp, model_params, cosmo_params):
    return 1 / hubble_6(zp, model_params, cosmo_params)


@lru_cache(maxsize=1024 * 1024)
def d_vz(z, model_params, cosmo_params):
    """Dilation scale for rbao size """
    int2, error = integrate.quad(d_vz_integrand, 0, z,
                                 epsabs=QUAD_EPSABS,
                                 args=(model_params, cosmo_params))
    int3 = (z / hubble_6(z, model_params, cosmo_params)) ** (1. / 3)
    return int3 * int2 ** (2 / 3)


@lru_cache(maxsize=1024 * 1024, typed=True)
def rBAO(z, model_params, cosmo_params):
    """BAO scale at redshit z """
    return (
        r_s(ZDRAG, model_params, cosmo_params) /
        d_vz(z, model_params, cosmo_params)
    )

# =============== SNeIa distances definition    =================
# ===============           SNeIa               =================


def distance_SNe_integrand(z, model_params, cosmo_params):
    h_z_in_Mpc = hubble_6(z, model_params, cosmo_params)*H0p/100
    return 1 / h_z_in_Mpc


def distance_SNe(z, model_params, cosmo_params):
    '''
    Luminosity distance for SNe data
    :return: (1+z) * Int_0^z dz E^-1(z; args) where E(z) = H(z)/H0
    '''
    int, err = integrate.quad(distance_SNe_integrand, 0, z,
                              epsabs=QUAD_EPSABS,
                              args=(model_params, cosmo_params))
    return (1 + z) * int


def mu_SNe(z, model_params, cosmo_params):
    '''
     Modulus distance for SNe data: 5*log10 of dist_lum(z)
    :return: 5*Log10(distance_SNe in Mpc) + 25
    '''
    h, OmegaM = cosmo_params
    cosmo_params = h, OmegaM
    d = distance_SNe(z, model_params, cosmo_params)
    mu0 = 25
    return 5 * np.log10(d) + mu0
