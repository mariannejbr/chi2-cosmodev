import os

import numpy as np

current_dir = os.path.abspath(os.path.dirname(__file__))
BASE_PATH = os.path.join(current_dir, '..', 'output', 'chains')

# Add library to PythonPath
# sys.path.insert(0, os.path.join(current_dir, '..'))

from Statistics.mcmc import (parallel_markov_chain,
                                       export_chains)


def run_parallel_mjb():
    """

    :return:
    """
    num_chains = 4

    w0ini = np.random.normal(-1, 0.2, num_chains)
    w1ini = np.random.normal(-1, 0.8, num_chains)
    qini = np.random.uniform(1, 10, num_chains)
    ztini = np.random.uniform(0, 3, num_chains)
    hini = np.random.normal(0.65, 0.2, num_chains)
    omini = np.random.normal(0.3, 0.1, num_chains)

    ini_step1 = [w0ini[0], w1ini[0], qini[0], ztini[0], hini[0], omini[0]]
    ini_step2 = [w0ini[1], w1ini[1], qini[1], ztini[1], hini[1], omini[1]]
    ini_step3 = [w0ini[2], w1ini[2], qini[2], ztini[2], hini[2], omini[2]]
    ini_step4 = [w0ini[3], w1ini[3], qini[3], ztini[3], hini[3], omini[3]]

    width = [0.05, 0.05, 0.1, 0.01, 0.01, 0.01]
    n_steps = 1000
    which = 1

    markov_chains_spec = [
        [n_steps, width, ini_step1, which],
        [n_steps, width, ini_step2, which],
        [n_steps, width, ini_step3, which],
        [n_steps, width, ini_step4, which],
    ]

    exec_results = parallel_markov_chain(markov_chains_spec)

    export_chains(BASE_PATH, markov_chains_spec, exec_results)


def run_parallel():
    """

    :return:
    """
    num_chains = 4

    w0ini = np.random.normal(-1, 0.2, num_chains)
    w1ini = np.random.uniform(-1, 1, num_chains)
    # w1ini = np.random.normal(-1, 0.8, num_chains)
    qini = np.random.uniform(1, 10, num_chains)
    ztini = np.random.uniform(0.01, 3, num_chains)
    hini = np.random.uniform(0.5, 1, num_chains)
    omini = np.random.uniform(0.1, 1, num_chains)
    # hini = np.random.normal(0.65, 0.2, num_chains)
    # omini = np.random.normal(0.3, 0.1, num_chains)

    ini_steps_set = np.stack(
            (w0ini, w1ini, qini, ztini, hini, omini), axis=-1
    )

    width = [0.05, 0.05, 0.1, 0.01, 0.01, 0.01]
    n_steps = 1000
    which = 1

    width_set = [width] * num_chains
    steps_set = [n_steps] * num_chains
    which_set = [which] * num_chains

    markov_chains_spec = list(
            zip(steps_set, width_set, ini_steps_set, which_set)
    )

    exec_results = parallel_markov_chain(markov_chains_spec)

    export_chains(BASE_PATH, markov_chains_spec, exec_results)


# Guard to avoid execution of run() function when this
# module is imported from another module.
if __name__ == '__main__':
    run_parallel()
