import datetime
import os
import sys

from scipy.optimize import differential_evolution

# ----- ADD THIS TO IMPORT FUNCTIONS FROM OTHER FILES
file_path = os.path.dirname(os.path.abspath(__file__))
print(file_path)
sys.path.insert(0, os.path.join(file_path, '..'))
from Constants_Units import *
from Input.SNeIa_Union2_1 import REDSHIFTS_SNe
from Input.bao_data import REDSHIFTS_RBAO_7new  # TODO add latest DR
from Input.hz_data import REDSHIFTS_Hz  # TODO newer data available??
from Statistics.chi2_likelihood import chi2SNe, chi2hz, chi2BAOCMB, \
    chi2BAOSNeHz, chi2BAOSNeHzCMB, chi2BAO
## ---->>> STEP 0
## ---->>>  CHOOSE THE EQUATION OF STATE <<<-------
from wz_gus import wz

HSneU21 = 0.742  # SNeIa Union 2.1 global value

# TODO: Add the header to the w_z file and import here the corresponding w(z)

Hz = 0
SNe = 1
BAOCMB = 2
BAOSNeHz = 3
Total = 4
BAO = 5

chi2_set = (
    ('Hz', chi2hz, len(REDSHIFTS_Hz), '> H(z)  (28)'),
    ('SNe', chi2SNe, len(REDSHIFTS_SNe), '> SNe U2.1 (557)'),
    # ---->>> TODO change number 3 to 4 if full CMB wang matrix is being used
    ('BAOCMB', chi2BAOCMB, 3 + len(REDSHIFTS_RBAO_7new),
     '> BAO (7) & CMB:Red Planck 2015 (3)'),
    ('BAOSNeHz', chi2BAOSNeHz,
     len(REDSHIFTS_RBAO_7new) + len(REDSHIFTS_SNe) + len(REDSHIFTS_Hz),
     '> BAO (7) + SNe U2.1 (557) +  H(z)  (28) '),
    ('Total', chi2BAOSNeHzCMB,
     len(REDSHIFTS_RBAO_7new) + len(REDSHIFTS_SNe) + len(REDSHIFTS_Hz) + 3,
     '> BAO (7) + SNe U2.1 (557) +  H(z)  (28) + CMB Red Planck 2015 (3)'),
    ('BAO', chi2BAO, len(REDSHIFTS_RBAO_7new),
     '> BAO = 6dF,DR7,DR13-1,DR13-3,SDSS(R),LyA1 ,LyA2'),

)
# ---->>> STEP 1 / 7
# ---->>> The single place to choice the data we want to use:
CHOICE_LIKELIHOOD = Total


def func_to_minimize(x, which=CHOICE_LIKELIHOOD):
    """
    We are minimizing chi2_func(x) at a given point, x.
    dim(x) = dim(w_params) + dim(cosmo_params)
    w_params = w0, w1, w2..., wn
    cosmo_params = h, omegab, omegac
    :param x:  The point in n-dimensional space.
    :param which: The dataset are we using.
        Options are stored in a dictionary:
        chi2_set = (

            ('Hz', chi2hz),
            ('SNe', chi2SNe),
            ('BAOCMB', chi2BAOCMB),
            ('BAO-SNe-Hz', chi2BAOSNeHz),
            ('Total', chi2HzCMBSNeBAO),
            ('BAO', chi2BAO),
            )
    :return:
    """
    # ---->>> STEP 2 / 7
    # ---->>> Fix some values if necessary
    # ---->>> a) If not using CMB data we need to fix to Planck BFV
    # omegabh2 = OMEGABH2
    # ---->>> b) SNeU21 alone cannot constrain H0:
    # hvar = HSneU21
    # ---->>> c) Test for CC wgus: fix omegach2
    # omegach2 = OMEGACH2
    # ---->>> d) Quintessence case n = 0
    w1 = 0
    # ---->>> e) Exponential case n = 1, C=1
    # w1 = 1
    # w2 = 1
    # ---->>> f) LCDM: A = 0
    # w0 = 0
    # w1 = 1
    # w2 = 1
    # ---->>> STEP 3 / 7
    # ---->>> Define the space parameter
    # w0, w1, w2, hvar, omegabh2, omegach2 = x
    # w0, w1, w2, hvar, omegach2 = x
    # w0, w1, w2, omegach2 = x
    # w0, w1, w2, hvar = x
    # w0, w2, omegach2 = x
    # w0, w2, hvar, omegach2 = x
    w0, w2, hvar, omegabh2, omegach2 = x
    # w0, hvar, omegabh2, omegach2 = x
    # w0, hvar, omegach2 = x
    # w0, omegach2 = x
    # omegach2, = x
    # hvar, omegach2 = x
    # hvar, omegabh2, omegach2 = x

    # ---->>> Map the parameters two types / No need to change
    w_params = w0, w1, w2
    cosmo_params = hvar, omegabh2, omegach2

    chi2name, chi2func, ndata, comment = chi2_set[which]

    val = chi2func(w_params, cosmo_params)
    print("Point:", x, "Function value:", val)
    return val


def chi2_minimization(which=CHOICE_LIKELIHOOD):
    '''
    ---->>> Bounds = [(w0), (w1), (w2),.., (h), (omegabh2), (omegach2)]

    Info:
    Priors used for Planck:
        omega_b*h^2 = (0.005, 0.10)
        omega_c*h^2 = (0.001, 0.99)

    BOUNDS MADE TO SECURE OMEGA_M<1
        h in (0.5, 0.8), to avoid OmegaM > 1
        baryons in (0.005, 0.05) to avoid OmegaM > 1
        CDM in (0.001, 0.2) to avoid OmegaM > 1


    :param which: Set of data to be constraint (Hz, SNe, BAO, etc)
    :return:
    '''
    # ---->>> Bounds for w_params and cosmo_params
    chi2name, chi2func, n_data, comment = chi2_set[which]
    print('Using dataset:', chi2name)
    # ---->>> STEP 4 / 7
    # ---->>> Repeat fixed parameters & Set Bounds for free parameters
    # omegabh2 = OMEGABH2
    # omegach2 = OMEGACH2
    # hvar = HSneU21
    # w0 = 0
    w1 = 0  # n = 0 quintessence
    # w1 = 1 # n = C = 1 exponential
    # w2 = 1  # n = C = 1 exponential
    # ---->>> Bounds: [free_w_params, h, omegabh2, omegach2]
    # ---->>> ****** Reminder: A = w0, n = w1, C = w2 *****
    # ---->>> A in (-50, 50), n in (0, 1), C in (-5, 5)
    ######### NOTE: to avoid degeneration in n=0 when A=0 we change bounds to:
    # ---->>> A in (0, 50), n =0, C in (-5, 0)
    # bounds = [(-50, 50), (0, 1), (-5, 5), (0.5, 0.8), (0.005, 0.045),(0.001, 0.2)]
    # bounds = [(-50, 50), (0, 1), (-5, 5), (0.5, 0.8), (0.001, 0.2)]
    # bounds = [(-50, 50), (0, 1), (-5, 5), (0.5, 0.8)]
    # bounds = [(-50, 50), (-5, 5), (0.001, 0.2)]
    # bounds = [(0, 50), (-5, 0), (0.001, 0.2)]  # !!! SEE NOTE in line 151 !!!!
    # bounds = [(0, 50), (-5, 0), (0.5, 0.8), (0.001, 0.2)]  # !!! SEE NOTE
    bounds = [(-50, 50), (-5, 5), (0.5, 0.8), (0.005, 0.045),
              (0.001, 0.2)]  # !SEE NOTE
    # bounds = [(-50, 50), (-5, 5), (0.5, 0.8), (0.001, 0.2)]
    # bounds = [(-50, 50), (-5, 5), (0.5, 0.8), (0.005, 0.045), (0.001, 0.2)]
    # bounds = [(-50, 50), (0.5, 0.8), (0.005, 0.045), (0.001, 0.2)]
    # bounds = [(-50, 50), (0.5, 0.8), (0.001, 0.2)]
    # bounds = [(-50, 50), (0.001, 0.2)]
    # bounds = [(0.5, 0.8), (0.005, 0.045), (0.001, 0.2)]
    # bounds = [(0.5, 0.8), (0.001, 0.2)]
    # bounds = [(0.001, 0.2)]

    t0 = datetime.datetime.now()

    result = differential_evolution(func_to_minimize,
                                    bounds=bounds,
                                    tol=1e-3,
                                    disp=True,
                                    # Specify `seed` for repeatable minimizations.
                                    seed=1,
                                    polish=False)

    total = datetime.datetime.now() - t0

    print(result)
    print("Total time", total)
    print("File name ", )
    chi2 = result.fun
    # ---->>> STEP 5 / 7
    # ---->>> Extract the results from the vector of parameters
    # w0, w1, w2, hvar, OmegaM = result.x
    # w0, w1, w2, hvar, omegabh2, omegach2 = result.x
    # w0, w1, w2, hvar = result.x
    # w0, w1, w2, omegach2 = result.x
    # w0, w2, omegach2 = result.x
    # w0, w2, hvar, omegach2 = result.x
    w0, w2, hvar, omegabh2, omegach2 = result.x
    # w0, hvar, omegabh2, omegach2 = result.x
    # w0, hvar, omegach2 = result.x
    # w0, omegach2 = result.x
    # hvar, omegach2 = result.x
    # omegach2, = result.x
    # hvar, omegabh2, omegach2 = result.x
    # w0, w1, w2, hvar, omegach2 = result.x
    # w0, w1, w2, hvar = result.x
    # w0, w1, w2, hvar, omegabh2, omegach2 = result.x

    # ---->>> Map the parameters two types / No need to change
    w_params = w0, w1, w2
    cosmo_params = hvar, omegabh2, omegach2
    # ========== NO NEED TO CHANGE THE FOLLOWING =========================
    wtoday = wz(0, w_params)
    mparams = len(result.x)
    OmegaM = (omegabh2 + omegach2) / hvar ** 2
    # ---->>>  goodness of fit as GammaInc(nu/2, chi2min/2)/Gamma(nu/2)
    # ---->>>  nu = the number of dof, i.e. nu = data - parameters = N - m
    ndata = n_data
    nu = ndata - mparams
    chi2red = chi2func(w_params, cosmo_params) / nu
    # ---->>> BIC criteria defined as BIC = -2ln(LikeMax) + mln(N)
    # ---->>> AIC criteria defined as AIC = - 2 ln(LikeMax) + 2*m
    bic = chi2 + mparams * np.log(ndata)
    aic = chi2 + 2 * mparams

    full_result = np.array(
        [wtoday, chi2, chi2red, w0, w1, w2,
         hvar, omegabh2, omegach2, OmegaM, aic, bic, mparams])

    now = datetime.datetime.now()
    date_str = now.strftime('%Y%m%d-%H%M%S')
    chi2name, chi2func, n_data, comment = chi2_set[which]
    ## ================================================================
    # ---->>> STEP 6 / 7
    # ---->>> Make sure the Header is correct
    HEADER = '''
         w(z) = -1 - A*exp(-z)*(z**n- z - C)
            * * * QUINTESSENCE-LIKE : n = 0  * * *
        ''
        if w1 == 0:
        
            return -1 + np.exp(-z) * (w0*z + w2)
        ''    
        
        C' = A*C
        A in [-50,50] and C' in [-5,5]
          
        Results are displayed in the following order: 
        w(z=0), chi2min, chi2reduced,
        w_params, h, omegabh2, omegach2, OmegaM,
        aic, bic, m_params

        bounds = [(A),       (n),    (C),     (h),      (omegabh2),    (omegach2)]
        bounds = [(0, 50),   (1), (-5, 0), (0.5, 0.8), (0.005, 0.045), (0.001, 0.2)]

        '''
    # ---->>> STEP 7 / 7
    # ---->>> If needed, rename the file with results
    # filetxt = '../Output/BFV/{:s}/test-cmb_{:s}.txt'.format(
    filetxt = '../Output/BFV/{:s}/{:s}_wg-n_0-cp-{:s}.txt'.format(
        chi2name,
        chi2name,
        date_str)

    np.savetxt(filetxt, full_result, header=HEADER)

    # print("File was saved in ", filetxt)


if __name__ == '__main__':
    chi2_minimization()
