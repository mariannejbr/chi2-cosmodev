import datetime
import os
import sys
from h5py import File

file_path = os.path.dirname(os.path.abspath(__file__))
print(file_path)
sys.path.insert(0, os.path.join(file_path, '..'))

from Input.bao_data import REDSHIFTS_RBAO_7new  # newest data
from Statistics.chi2_likelihood import *

## ========= DICTIONARY OF CHI2 DATA SETS =============
Hz = 0
SNe = 1
BAOCMB = 2
BAOSNeHz = 3
Total = 4
BAO = 5

chi2_set = (
    ('Hz', chi2hz, len(REDSHIFTS_Hz), '> H(z)  (28)'),

    ('SNe', chi2SNe, len(REDSHIFTS_SNe), '> SNe U2.1 (557)'),

    ('BAOCMB', chi2BAOCMB, 3 + len(REDSHIFTS_RBAO_7new),
     '> BAO (7) & CMB:Red Planck 2015 (3)'),

    ('BAOSNeHz', chi2BAOSNeHz,
     len(REDSHIFTS_RBAO_7new) + len(REDSHIFTS_SNe) + len(REDSHIFTS_Hz),
     '> BAO (7) + SNe U2.1 (557) +  H(z)  (28) '),

    ('Total', chi2BAOSNeHzCMB,
     len(REDSHIFTS_RBAO_7new) + len(REDSHIFTS_SNe) + len(REDSHIFTS_Hz) + 3,
     '> BAO (7) + SNe U2.1 (557) +  H(z)  (28) + CMB Red Planck 2015 (3)'),

    ('BAO', chi2BAO, len(REDSHIFTS_RBAO_7new),
     '> BAO = 6dF,DR7,DR13-1,DR13-3,SDSS(R),LyA1 ,LyA2'),

)
## ===========================================================================
## -------------------         12 STEPS PROGRAM        -----------------------
## ===========================================================================
##  Follow the steps to adapt this script and create each chi2 grid evaluation
##  This script will generate a .h5 file with
##  [dim_x, dim_y, chi2(w_params, cosmo_params)]
##  where dim_x and dim_y are, in general, belonging to w_params or cosmo_params
##  ============================================================================

## =========                STEP 1/12            =============
## ========= WHICH DATA SET ARE YOU USING?  =============
# The single place to choice the data we want to use:
CHOICE_LIKELIHOOD = Total

## =========                STEP 2/12            =============
## ========= IMPORT THE BFVs FOR THE CHOSEN CHI2 =============
## The name of the file should be added manually. Copy/paste is recommended to
# avoid mispelling errors

chi2name, chi2func, ndata, comment = chi2_set[CHOICE_LIKELIHOOD]
BFV_filename = '../Output/BFV/{:s}/wg-20200722-142341.txt'.format(
    chi2name)
BFV_file = np.loadtxt(BFV_filename)

print('We are using the file: ', BFV_filename)
print('======================== : ) ==========================')
print(BFV_file)
## >>> We unpack the values of the BFV file in order:
(wztoday, chi2b, chi2red,
 w0b, w1b, w2b,
 hb, omegabh2b, omegach2b, OmegaMb,
 bic, aic, mparams
 ) = BFV_file


# DO NOT TOUCH
def chi2_kernel(data, which=CHOICE_LIKELIHOOD):
    chi2name, chi2func, ndata, comment = chi2_set[which]

    w0, w1, w2, h, omegab, omegac = data
    # w0, w1, w2, h, OmegaM = data
    w_params = w0, w1, w2
    cosmo_params = h, omegab, omegac
    OmegaM = omegac / h ** 2 + omegab / h ** 2
    chi = chi2func(w_params, cosmo_params)
    # print("Completed chi2 for", data)
    # print("Value:", chi)
    return chi


def exec_chi2_grid(which=CHOICE_LIKELIHOOD):
    ## =========                STEP 3/12            =============
    ## ======== SELECT THE NUMBER OF EVALUATIONS PER GRID DIMENSION
    ## >>>>>> keep in mind that we will execute N = steps * steps evaluations
    steps = 300

    ## =========== FROM THE FREE PARAMETERS WE CHOOSE WHICH ARE GONNA VARY ====
    ## Commented out means it's fixed, otherwise we specify how many values do we want
    # TODO: refactor this in such a way that we name them dim_x and dim_y?

    ## =========                STEP 4/12            =============

    w0_values = steps # A
    w1_values = steps # n
    # w2_values = steps # C
    # h_values = steps
    # ombh2_values = steps
    # omch2_values = steps
    # TODO add OmegaM as a dimension in grid
    # omMatter_values = steps

    ## =========                STEP 5/12            =============
    ## ======== Now let's specify the range for each dimension of the grid
    ## Important/useful to know which was the BFV for each parameter so we
    ## try to center it in the range
    w0_range = np.linspace(-10, 10, w0_values)
    # w2_range = np.linspace(-5, 5, w2_values)
    # TODO: check the following values
    #### WARNING:  to avoid problems in Omega_M value we
    ####           kept the bounds for h, om_b and om_c
    ####           (0.5, 0.8), (0.005, 0.05),(0.001, 0.2)
    ####           respectively
    # h_range = np.linspace(0.5, 1.1, h_values)
    # omegabh2_range = np.linspace(0.005, 0.05, ombh2_values)
    # omegach2_range = np.linspace(0.001, 0.20, omch2_values)
    # omegaMatter_range = np.linspace(0.2, 0.6, omMatter_values)

    ## =========                STEP 6a/12            =============
    ## We create an empty n-dimensional grid to store the values of chi2 where n = m_params
    ## In the most general case, m_params = 6 so we use the highest value for all the grids
    grid_data = np.zeros((w0_values, w1_values, 6))
    # grid_data = np.zeros((w0_values, w2_values, 6))
    # grid_data = np.zeros((w1_values, w2_values, 6))
    # grid_data = np.zeros((w0_values, h_values, 6))
    # grid_data = np.zeros((w1_values, h_values, 6))
    # grid_data = np.zeros((w2_values, h_values, 6))
    # grid_data = np.zeros((w0_values, omch2_values, 6))
    # grid_data = np.zeros((w1_values, omch2_values, 6))
    # grid_data = np.zeros((w2_values, omch2_values, 6))
    # grid_data = np.zeros((h_values, ombh2_values,6))
    # grid_data = np.zeros((h_values, omch2_values,6))

    ## =========                STEP 6b/12            =============
    ## We do either 6a or 6b but not both at the same time
    ## UNIDIMENSIONAL GRID for chi2(one_param) exploration
    ## this is used for quoting the errors per parameter.
    # grid_data = np.zeros((w0_values, 6))
    # grid_data[:, :, 2:] = BEST_FIT[2:]

    ## =========                STEP 7/12            =============
    ## ======== Let's fix the values for the rest of the parameters.
    ## Those that we are not varying in this case, we fix to the best fit value!

    # grid_data[:, :, 0] = w0b  # subscript b indicates bestfitvalue
    # grid_data[:, :, 1] = w1b
    grid_data[:, :, 2] = w2b
    grid_data[:, :, 3] = hb
    grid_data[:, :, 4] = omegabh2b
    grid_data[:, :, 5] = omegach2b
    ### TODO: how to use Omega_M instead of omegach2/omegabh2?

    ## =========                STEP 8.1/12            =============
    ## =========  Assign the values of dim_x  in the grid array

    # First element (index 0) in array corresponds to w0 values
    # NOTICE: we have to add a new axis in w0_range
    grid_data[:, :, 0] = w0_range[:, np.newaxis]
    # grid_data[:, :, 1] = w1_range[:, np.newaxis]
    # grid_data[:, :, 2] = w2_range[:, np.newaxis]
    # grid_data[:, :, 3] = h_range[:, np.newaxis]
    # grid_data[:, :, 4] = omegabh2_range[:, np.newaxis]
    # grid_data[:, :, 5] = omegach2_range[:, np.newaxis]
    # TODO: Implement a smarter way to assign the dimensions to the matrix
    #       for instance; dim(w_params) & dim(cosmo_params)

    ## =========                STEP 8.2/12            =============
    ## =========  Assign the values of dim_y  in the grid array

    # First element (index 1) in array corresponds to wi values
    # NOTICE: we have to add a new axis in w0_range
    # grid_data[:, :, 0] = w0_range[np.newaxis, :]
    grid_data[:, :, 1] = w1_range[np.newaxis, :]
    # grid_data[:, :, 2] = w2_range[np.newaxis, :]
    # grid_data[:, :, 3] = h_range[np.newaxis, :]
    # grid_data[:, :, 4] = omegabh2_range[np.newaxis, :]
    # grid_data[:, :, 5] = omegach2_range[np.newaxis, :]

    ## =========                STEP 9/12            =============
    ## =========  Flatten array into (dim_x * dim_y, m_params)

    flattened_grid = grid_data.reshape(w0_values * w1_values, 6)
    # flattened_grid = grid_data.reshape(w0_values * w2_values, 6)
    # flattened_grid = grid_data.reshape(w1_values * w2_values, 6)
    # flattened_grid = grid_data.reshape(w0_values * h_values, 6)
    # flattened_grid = grid_data.reshape(w1_values * h_values, 6)
    # flattened_grid = grid_data.reshape(w2_values * h_values, 6)
    # flattened_grid = grid_data.reshape(w0_values * omch2_values, 6)
    # flattened_grid = grid_data.reshape(w1_values * omch2_values, 6)
    # flattened_grid = grid_data.reshape(w2_values * omch2_values, 6)
    # flattened_grid = grid_data.reshape(h_values * ombh2_values, 6)
    # flattened_grid = grid_data.reshape(h_values * omch2_values, 6)

    ## =========                STEP 10/12            =============
    ## =========  Execute parallel routine
    # map the kernel into the flattened grid (NOTHING to be changed here)
    chi2func_data = parallel_chi2any(chi2_kernel, flattened_grid)

    ## =========                STEP 11/12            =============
    ## ========= Reshape chi2 results with a similar shape as the original
    #            data_grid, but with only one element.¡: the chi squared.

    chi2func_data = chi2func_data.reshape(w0_values, w1_values, 1)
    # chi2func_data = chi2func_data.reshape(w0_values, w2_values, 1)
    # chi2func_data = chi2func_data.reshape(w1_values, w2_values, 1)
    # chi2func_data = chi2func_data.reshape(w0_values, h_values, 1)
    # chi2func_data = chi2func_data.reshape(w1_values, h_values, 1)
    # chi2func_data = chi2func_data.reshape(w2_values, h_values, 1)
    # chi2func_data = chi2func_data.reshape(w0_values, omch2_values, 1)
    # chi2func_data = chi2func_data.reshape(w1_values, omch2_values, 1)
    # chi2func_data = chi2func_data.reshape(w2_values, omch2_values, 1)
    # chi2func_data = chi2func_data.reshape(h_values, omch2_values, 1)

    now = datetime.datetime.now()
    date_str = now.strftime('%Y%m%d-%H%M%S')
    chi2name, chi2func, n_data, comment = chi2_set[which]

    ## =========                STEP 12/12            =============
    # Save data to file. Use a sensible name for the file.
    filename = './../Output/grids/{:s}/wgus_A-n_{:s}.h5'.format(
        chi2name,
        date_str)
    with File(filename, 'w') as file:

        try:
            # Try to delete existent data
            del file['/DataGrid']
            del file['/Chi2']
        except KeyError:
            # If data does not exist, do nothing
            pass

        file.attrs['Comment'] = '''
        Grid created from the best fit values
        contained in the following file:      
        '''
        str(BFV_file)
        # Add data
        file['/DataGrid'] = grid_data
        file['/Chi2'] = chi2func_data

        # Save data to file
        file.flush()

        print("    ")
        print("File saved to ", filename)


if __name__ == '__main__':
    exec_chi2_grid()
