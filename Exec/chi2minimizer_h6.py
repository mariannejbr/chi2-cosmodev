import datetime
import os
import sys

import numpy as np
from scipy.optimize import differential_evolution

# add this line first to import the modules afterwards
file_path = os.path.dirname(os.path.abspath(__file__))
print(file_path)
sys.path.insert(0, os.path.join(file_path, '..'))
from Constants_Units import REDUCED_H0, OMEGAM0
from Input.hz_data import REDSHIFTS_Hz
from Input.SNeIa_Union2_1 import REDSHIFTS_SNe
from chi2_cosmo_dev.chi2_h6 import chi2hz, chi2SNe, chi2CCSNe

HEADER = '''
Union 2.1
model: beta free to vary within 10^-10 and 1
h and Omega_m fixed to Planck cosmology

bounds in the order (h, om_m, beta):

 = [(0, 1), (0, 1), (0, 1e-10)]

Results are displayed in the following order: 
chi2min, chi2reduced,
beta, h, OmegaM, aic, bic
'''

def chi2_minimization():
    # bounds for model_params and cosmo_params
    #hvar, OmegaM = REDUCED_H0, OMEGAM0
    bounds = [(0.01, 0.99), (0.01, 0.99), (0, 1e-10)]
    #bounds = [(0, 1e-2)]
    def func_to_minimize(x):
        """
        :param x:  The point.
        :return:
        """
    #    beta, = x
        hvar, OmegaM, beta, = x
        # hvar, OmegaM = x
        model_params = beta,
        cosmo_params = hvar, OmegaM

        #val = chi2hz(model_params, cosmo_params)
        #val = chi2SNe(model_params, cosmo_params)
        val = chi2CCSNe(model_params, cosmo_params)
        print("Point:", x, "Function value:", val)
        # print("Function value:", val)
        return val

    t0 = datetime.datetime.now()

    result = differential_evolution(func_to_minimize,
                                    bounds=bounds,
                                    tol=1e-3,
                                    disp=True,
                                    #maxiter=2000,
                                    #seed=10,
                                    polish=True
                                    )

    total = datetime.datetime.now() - t0

    print(result)
    print("Total time", total)

    chi2 = result.fun

    #beta = result.x
    hvar, OmegaM, beta = result.x

    model_params = beta,

    cosmo_params = hvar, OmegaM

    # # goodness of fit as GammaInc(nu/2, chi2min/2)/Gamma(nu/2)
    # # nu = the number of dof, i.e. nu = data - parameters = N - m

    #n_data = len(REDSHIFTS_Hz)
    n_data = len(REDSHIFTS_SNe) + len(REDSHIFTS_Hz)
    m_params = len(result.x)
    nu = n_data - m_params

    chi2red = chi2 / nu

    # # BIC criteria defined as BIC = -2ln(LikeMax) + mln(N)
    bic = chi2 + m_params * np.log(n_data)

    # # AIC criteria defined as AIC = - 2 ln(LikeMax) + 2*m
    aic = chi2 + 2 * m_params

    full_result = np.array(
        [chi2, chi2red,
         beta, hvar, OmegaM,
         bic, aic
         ])

    now = datetime.datetime.now()
    date_str = now.strftime('%Y%m%d-%H%M%S')
    path_to_output = './../chi2_cosmo_dev/output/bfv/'
    file_txt = path_to_output + '{:s}-chi2SNeU21-CC_beta_h_OmM.txt'.format(
        date_str
    )

    np.savetxt(file_txt, full_result,
               header=HEADER)


if __name__ == '__main__':
    chi2_minimization()
