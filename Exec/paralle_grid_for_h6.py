import datetime
import sys, os
import numpy as np
from h5py import File

file_path = os.path.dirname(os.path.abspath(__file__))
print(file_path)
sys.path.insert(0, os.path.join(file_path, '..'))
from chi2_cosmo_dev.chi2_h6 import chi2hz, chi2SNe, chi2CCSNe, parallel_chi2any

#BFV_file = np.loadtxt('./../chi2_cosmo_dev/output/bfv/20190403-173034-chi2Hz_beta_h_OmM.txt')
BFV_file = np.loadtxt('./../chi2_cosmo_dev/output/bfv/20190403-184022-chi2SNeU21_beta_h_OmM.txt')
#BFV_file = np.loadtxt('./../chi2_cosmo_dev/output/bfv/20190403-190211-chi2SNeU21-CC_beta_h_OmM.txt')

print(BFV_file)

(chi2b, chi2red,
 betab,
 hb, OmegaMb,
 bic, aic
 ) = BFV_file


def chi2Total_kernel(data):
    beta, h, OmegaM = data
    model_params = beta,
    cosmo_params = h, OmegaM

    #chi = chi2hz(model_params, cosmo_params)
    chi = chi2SNe(model_params, cosmo_params)
    #chi = chi2CCSNe(model_params, cosmo_params)

    # print("Completed chi2 for", data)
    # print("Value:", chi)
    return chi


def exec_chi2any():
    steps = 100
    beta_values = steps
    #h_values = steps
    omMatter_values = steps

    # Let's fix the values for the rest of the parameters.
    # beta_range = np.array([betab])
    h_range = np.array([hb])
    # omegaMatter_range = np.array([OmegaMb])

    # Now let's put some values to beta
    #beta_range = np.linspace(0, 1e-2, beta_values)
    beta_range = np.logspace(-10, 0, beta_values)
    #h_range = np.linspace(0.55, 0.9, h_values)
    omegaMatter_range = np.linspace(0, 0.5, omMatter_values)

    grid_data = np.zeros((beta_values, omMatter_values, 3))
    # grid_data = np.zeros((beta_values, h_values, 3))
    #grid_data = np.zeros((h_values, omMatter_values, 3))

    # grid_data[:, :, 0] = betab
    grid_data[:, :, 1] = hb
    #grid_data[:, :, 2] = OmegaMb

    # Assign the values in the grid array

    # First element (index 0) in array corresponds to beta values
    # NOTICE: we have to add a new axis in w0_range
    grid_data[:, :, 0] = beta_range[:, np.newaxis]
    #grid_data[:, :, 1] = h_range[:, np.newaxis]
    # grid_data[:, :, 2] = omegaMatter_range[:, np.newaxis]

    # First element (index 1) in array corresponds to wi values
    # NOTICE: we have to add a new axis in w0_range

    # grid_data[:, :, 1] = h_range[np.newaxis, :]
    grid_data[:, :, 2] = omegaMatter_range[np.newaxis, :]

    # Flatten array

    flattened_grid = grid_data.reshape(beta_values * omMatter_values, 3)
    #flattened_grid = grid_data.reshape(h_values * omMatter_values, 3)
    #flattened_grid = grid_data.reshape(beta_values * h_values, 3)

    # Execute parallel routine
    chi2Total_data = parallel_chi2any(chi2Total_kernel, flattened_grid)

    # Reshape chi2BAO results with a similar shape as the original
    # data_grid, but with only one element.¡: the chi squared.
    chi2Total_data = chi2Total_data.reshape(beta_values, omMatter_values, 1)
    #chi2Total_data = chi2Total_data.reshape(h_values, omMatter_values, 1)
    #chi2Total_data = chi2Total_data.reshape(beta_values, h_values, 1)

    now = datetime.datetime.now()
    date_str = now.strftime('%Y%m%d-%H%M%S')

    # Save data to file
    filename = './../chi2_cosmo_dev/output/grids/beta-OmM_Union21-grid-{:s}.h5'.format(
        date_str)
    with File(filename, 'w') as file:

        try:
            # Try to delete existent data
            del file['/DataGrid']
            del file['/Chi2']
        except KeyError:
            # If data does not exist, do nothing
            pass

        file.attrs['Comment'] = '''
        Grid created from the best fit values
        contained in the following file:
        20190403-184022-chi2SNeU21_beta_h_OmM.txt
        '''
        # Add data
        file['/DataGrid'] = grid_data
        file['/Chi2'] = chi2Total_data

        # Save data to file
        file.flush()

        # print(chi2_data)


if __name__ == '__main__':
    exec_chi2any()
