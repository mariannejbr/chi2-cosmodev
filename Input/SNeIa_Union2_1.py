import numpy as np
import os

cw_directory = os.path.dirname(os.path.abspath(__file__))
# print(cw_directory)


fname = os.path.join(cw_directory, 'union2.txt')

SNefile = np.loadtxt(fname, usecols=(1, 2, 3))

REDSHIFTS_SNe = SNefile[:, 0]
OBS_SNe = SNefile[:, 1]
ERROR_SNe = SNefile[:, 2] * SNefile[:, 2]
#square root of the errors to standarize with JLa

DATA_SNe = np.stack((REDSHIFTS_SNe, OBS_SNe, ERROR_SNe), axis=1)
#DATA_SNe = SNefile
