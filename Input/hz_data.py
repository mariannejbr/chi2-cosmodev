import numpy as np

import os

cw_directory = os.path.dirname(os.path.abspath(__file__))

fname= os.path.join(cw_directory,'Hznewdata.txt')

Hzfile = np.loadtxt(fname, usecols=(0, 1, 2))

REDSHIFTS_Hz = Hzfile[:, 0]
OBS_Hz = Hzfile[:, 1]
ERROR_Hz = Hzfile[:, 2]

#DATA_SNe = np.stack(REDSHIFTS_SNe, OBS_SNe, ERROR_SNe, axis=1)
DATA_Hz = Hzfile
