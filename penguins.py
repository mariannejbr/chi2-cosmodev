# -*- encoding: utf-8 -*-
import os
import os.path as path
import sys
from datetime import datetime

import matplotlib as mpl
import matplotlib.pyplot as pyplot
import numpy as np
import scipy.ndimage as ndimage
from h5py import File
from matplotlib.ticker import Formatter

current_dir = os.path.dirname(os.path.abspath(__file__))
print(current_dir)

#sys.path.insert(0, os.path.join(current_dir))
# path for files with bfv and grids data
BASE_PATH = path.join(current_dir, 'Output')



mpl.rcParams['xtick.labelsize'] = 20
mpl.rcParams['ytick.labelsize'] = 20
mpl.rcParams['mathtext.fontset'] = 'cm'

TOTALGRIDS_PATH = path.join(BASE_PATH, 'grids', 'Total')
TOTALBFV_PATH = path.join(BASE_PATH, 'BFV', 'Total')

SNeGRIDS_PATH = path.join(BASE_PATH, 'grids', 'SNe')
SNeBFV_PATH = path.join(BASE_PATH, 'BFV', 'SNe')

HzGRIDS_PATH = path.join(BASE_PATH, 'grids', 'Hz')
HzBFV_PATH = path.join(BASE_PATH, 'BFV', 'Hz')

BAOGRIDS_PATH = path.join(BASE_PATH, 'grids', 'BAO')
BAOBFV_PATH = path.join(BASE_PATH, 'BFV', 'BAO')


BASE_FIGURE_NAME = 'Contour'

data_origins = [
    # First figure, beta-Omega_m contour
    {
        'WqCDM': [
            # {
            #     'best_fit': np.loadtxt(path.join(
            #
            #         TOTALBFV_PATH, 'TOTAL-6params-20171104-163023.txt'
            #
            #     )),
            #     'data_grid': File(path.join(
            #
            #         TOTALGRIDS_PATH, 'w0-w2-20171104-173531.h5'
            #
            #     )),
            #
            #     'delta_chi2': np.array([7.038, 12.839, 20.062])
            #     # m = 6 parameters fitted
            #
            # }
{
                'best_fit': np.loadtxt(path.join(

                    HzBFV_PATH, 'Hz-f_R-4params-20171104-164907.txt'

                )),
                'data_grid': File(path.join(

                    HzGRIDS_PATH, '20171105-224705_w0-w2_fr4.h5'

                )),

                'delta_chi2': np.array([4.71947, 9.71563, 16.2513])
                # m = 4 parameters fitted

            }
            ,
            {
                'best_fit': np.loadtxt(path.join(

                    SNeBFV_PATH, 'SNe-fR-4params-20171104-165049.txt'

                )),
                'data_grid': File(path.join(

                    SNeGRIDS_PATH, '20171105-224407_w0-w2_fr4.h5'
                )),

                'delta_chi2': np.array([4.71947, 9.71563, 16.2513])
                # m = 4 parameters fitted
                #
            }
            ,
            {
                'best_fit': np.loadtxt(path.join(

                    BAOBFV_PATH, 'BAO-f_R_4params-20171104-165447.txt'
                )),

                'data_grid': File(path.join(
                    BAOGRIDS_PATH, '20171105-215708_w0-w2_fr4.h5'
                )),

                'delta_chi2': np.array([4.71947, 9.71563, 16.2513])
                # m = 4 parameters fitted

            }
            ,
            {
                'best_fit': np.loadtxt(path.join(

                    SNeBFV_PATH, 'SNe-fR-4params-20171104-165049.txt'

                )),
                'data_grid': File(path.join(

                    BAOGRIDS_PATH, '20171105-224407_w0-w2_fr4.h5'

                )),

                'delta_chi2': np.array([4.71947, 9.71563, 16.2513])
                #  m = 4 parameters fitted


            }
        ],

        'FIGURE_NAME': 'w0w2-Hz-BAO-SNe',
        'LEGEND': r'',

        'PLOT_LIMITS': [[-2, 0], [0, np.pi]]
    }
    # ,
    # #
    # Second figure, W1-W2 contour

    # {
    #     'WqCDM': [
    #         # {
    #         #     'best_fit': np.loadtxt(path.join(
    #         #
    #         #         TOTALBFV_PATH, 'TOTAL-6params-20171104-163023.txt'
    #         #
    #         #     )),
    #         #     'data_grid': File(path.join(
    #         #
    #         #         TOTALGRIDS_PATH, 'w0-w2-20171104-173531.h5'
    #         #
    #         #     )),
    #         #
    #         #     'delta_chi2': np.array([7.038, 12.839, 20.062])
    #         #     # m = 6 parameters fitted
    #         #
    #         # }
    #         {
    #             'best_fit': np.loadtxt(path.join(
    #
    #                 HzBFV_PATH, 'Hz-f_R-4params-20171104-164907.txt'
    #
    #             )),
    #             'data_grid': File(path.join(
    #
    #                 HzGRIDS_PATH, '20171105-224705_w0-w2_fr4.h5'
    #
    #             )),
    #
    #             'delta_chi2': np.array([4.71947, 9.71563, 16.2513])
    #             # m = 4 parameters fitted
    #
    #         }
    #         ,
    #         {
    #             'best_fit': np.loadtxt(path.join(
    #
    #                 SNeBFV_PATH, 'SNe-fR-4params-20171104-165049.txt'
    #
    #             )),
    #             'data_grid': File(path.join(
    #
    #                 SNeGRIDS_PATH, '20171105-224407_w0-w2_fr4.h5'
    #             )),
    #
    #             'delta_chi2': np.array([4.71947, 9.71563, 16.2513])
    #             # m = 4 parameters fitted
    #             #
    #         }
    #         ,
    #         {
    #             'best_fit': np.loadtxt(path.join(
    #
    #                 BAOBFV_PATH, 'BAO-f_R_4params-20171104-165447.txt'
    #             )),
    #
    #             'data_grid': File(path.join(
    #                 BAOGRIDS_PATH, '20171105-215708_w0-w2_fr4.h5'
    #             )),
    #
    #             'delta_chi2': np.array([4.71947, 9.71563, 16.2513])
    #             # m = 4 parameters fitted
    #
    #         }
    #         ,
    #         {
    #             'best_fit': np.loadtxt(path.join(
    #
    #                 SNeBFV_PATH, 'SNe-fR-4params-20171104-165049.txt'
    #
    #             )),
    #             'data_grid': File(path.join(
    #
    #                 BAOGRIDS_PATH, '20171105-224407_w0-w2_fr4.h5'
    #
    #             )),
    #
    #             'delta_chi2': np.array([4.71947, 9.71563, 16.2513])
    #             #  m = 4 parameters fitted
    #
    #         }
    #     ],
    #
    #     'FIGURE_NAME': 'w0w2-Hz-BAO-SNe',
    #     'LEGEND': r'',
    #
    #     'PLOT_LIMITS': [[-2, 0], [0, np.pi]]
    # }
    #,

    # THIRD FIGURE W2-W3
    # {
    #     'WqCDM': [
    #         # {
    #         #     'best_fit': np.loadtxt(path.join(
    #         #
    #         #         TOTALBFV_PATH, 'TOTAL-6params-20171104-163023.txt'
    #         #
    #         #     )),
    #         #     'data_grid': File(path.join(
    #         #
    #         #         TOTALGRIDS_PATH, 'w0-w2-20171104-173531.h5'
    #         #
    #         #     )),
    #         #
    #         #     'delta_chi2': np.array([7.038, 12.839, 20.062])
    #         #     # m = 6 parameters fitted
    #         #
    #         # }
    #         {
    #             'best_fit': np.loadtxt(path.join(
    #
    #                 HzBFV_PATH, 'Hz-f_R-4params-20171104-164907.txt'
    #
    #             )),
    #             'data_grid': File(path.join(
    #
    #                 HzGRIDS_PATH, '20171105-224705_w0-w2_fr4.h5'
    #
    #             )),
    #
    #             'delta_chi2': np.array([4.71947, 9.71563, 16.2513])
    #             # m = 4 parameters fitted
    #
    #         }
    #         ,
    #         {
    #             'best_fit': np.loadtxt(path.join(
    #
    #                 SNeBFV_PATH, 'SNe-fR-4params-20171104-165049.txt'
    #
    #             )),
    #             'data_grid': File(path.join(
    #
    #                 SNeGRIDS_PATH, '20171105-224407_w0-w2_fr4.h5'
    #             )),
    #
    #             'delta_chi2': np.array([4.71947, 9.71563, 16.2513])
    #             # m = 4 parameters fitted
    #             #
    #         }
    #         ,
    #         {
    #             'best_fit': np.loadtxt(path.join(
    #
    #                 BAOBFV_PATH, 'BAO-f_R_4params-20171104-165447.txt'
    #             )),
    #
    #             'data_grid': File(path.join(
    #                 BAOGRIDS_PATH, '20171105-215708_w0-w2_fr4.h5'
    #             )),
    #
    #             'delta_chi2': np.array([4.71947, 9.71563, 16.2513])
    #             # m = 4 parameters fitted
    #
    #         }
    #         ,
    #         {
    #             'best_fit': np.loadtxt(path.join(
    #
    #                 SNeBFV_PATH, 'SNe-fR-4params-20171104-165049.txt'
    #
    #             )),
    #             'data_grid': File(path.join(
    #
    #                 BAOGRIDS_PATH, '20171105-224407_w0-w2_fr4.h5'
    #
    #             )),
    #
    #             'delta_chi2': np.array([4.71947, 9.71563, 16.2513])
    #             #  m = 4 parameters fitted
    #
    #         }
    #     ],
    #
    #     'FIGURE_NAME': 'w0w2-Hz-BAO-SNe',
    #     'LEGEND': r'',
    #
    #     'PLOT_LIMITS': [[-2, 0], [0, np.pi]]
    # }

]


##########################################################

class FancyFormatter(Formatter):
    """Formater for values with LaTeX fonts."""

    def __call__(self, kS, pos=None):
        return r'${:.5G}$'.format(kS)

    def format_data_short(self, value):
        return '{:.1f}'.format(value)


def _extract(h5file):
    # return h5file['/DataGrid'], h5file['/Chi2BAO']
    return h5file['/DataGrid'], h5file['/Chi2']


def _build_figname(suffix=None):
    if suffix is None:
        return BASE_FIGURE_NAME
    return '{0}-{1}'.format(BASE_FIGURE_NAME, suffix)


def make_contour_plot(origin):
    dark_pink = '#800080'
    light_pink = '#FFB3FF'

    dark_blue = '#003366'
    light_blue = '#99CCFF'

    dark_orange = '#FF8000'
    light_orange = '#FFBF80'

    dark_green = '#208000'
    light_green = '#C6FFB3'

    dark_red = '#CC0000'
    light_red = '#DEA6A6'

    colors = [

        [dark_blue, light_blue],
        [dark_orange, light_orange],
        [dark_pink, light_pink],
        [dark_green, light_green],
        [dark_red, light_red]
    ]
    alphas = [
        [1, 0.5],
        [0.9, 0.6],
        [0.8, 0.7],
        [0.75, 0.6]
    ]

    figsize = (6, 6)  # Width, height inches

    pyplot.figure(figsize=figsize)

    ###

    for idx, WqCDM_data_set in enumerate(origin['WqCDM'][:]):
        print(">> Data used for contouring")
        print(idx, WqCDM_data_set)

        # WqCDM_best_fit = _extract(WqCDM_data_set['best_fit'])
        WqCDM_best_fit = WqCDM_data_set['best_fit']
        WqCDM_grid_data, WqCDM_chi2_data = _extract(WqCDM_data_set['data_grid'])

        delta_chi2 = WqCDM_data_set['delta_chi2']
        chi2min = WqCDM_best_fit[0]

        # h0 and OmegaM values from best fit
        # sh0, sOmM  = WqCDM_best_fit.take((5, 6))
        # pyplot.plot([sOmM], [sh0], 'ro')
        # WqCDM_contour_data = WqCDM_grid_data[0, 0, 0, 0, :, :, (4, 5, 6)]

        #H0 = WqCDM_grid_data[:, :, 4]
        W0 = WqCDM_grid_data[:, :, 0]
        #W1 = WqCDM_grid_data[:, :, 1]
        W2 = WqCDM_grid_data[:, :, 2]
        #W3 = WqCDM_grid_data[:, :, 3]

        # OmM = WqCDM_grid_data[:, :, 5]

        print(W0)
        print(W2)

        chi2 = WqCDM_chi2_data[:, :, 0]
        print(chi2)

        print(chi2[:, 25])

        chi2 = ndimage.interpolation.spline_filter(chi2, order=3)

        levels = chi2min + delta_chi2
        print(">> WqCDM levels:", levels)
        levels[0] = 0
        print(levels)
        # FOR FIRST FIGURE
        #pyplot.contour(W1, W2, chi2, levels=levels,
        pyplot.contour(W0, W2, chi2, levels=levels,
                       colors=colors[idx][0],  # colorscolors
                       linewidths=1, alpha=alphas[idx][0])

#        cs = pyplot.contourf(W1, W2, chi2, levels=levels,
        cs = pyplot.contourf(W0, W2, chi2, levels=levels,
                             colors=colors[idx], alpha=alphas[idx][1])
        # # FOR SECOND FIGURE
        #        pyplot.contour(W0, Wi, chi2, levels=levels,
        #                       colors=colors[idx][0], #colorscolors
        #                       linewidths=1, alpha=alphas[idx][0])
        #
        #        cs = pyplot.contourf(W0, Wi, chi2, levels=levels,
        #                        colors=colors[idx], alpha=alphas[idx][1])
        # # FOR THIRD FIGURE
        #        pyplot.contour(W0, OmM, chi2, levels=levels,
        #                       colors=colors[idx][0],  # colorscolors
        #                       linewidths=1, alpha=alphas[idx][0])
        #
        #        cs = pyplot.contourf(W0, OmM, chi2, levels=levels,
        #                             colors=colors[idx], alpha=alphas[idx][1])

    axes = pyplot.gca()
    axes.xaxis.set_major_formatter(FancyFormatter())
    axes.yaxis.set_major_formatter(FancyFormatter())
    # cbar = pyplot.colorbar(cs, label=r"$\chi^2$")
    # print(best_fit)
    # print(best_fit.reshape([2, 1]))

    pyplot.xlim(origin['PLOT_LIMITS'][0])
    pyplot.ylim(origin['PLOT_LIMITS'][1])

    # pyplot.legend(cs)
    pyplot.xlabel(r'$w_0$', fontsize=28)
    pyplot.ylabel(r'$w_2$', fontsize=28)
    # pyplot.ylabel(r'$w_i$', fontsize=28)
    # pyplot.ylabel(r'$\Omega_M$', fontsize=28)



    ## adapt coordinates to each figure
    pyplot.text(-1.75, 2.5, r'BAO+CC+SNeIa', fontsize=20,
                color=dark_blue)

    #pyplot.text(-0.75, 0.8, r'BAO+CMB', fontsize=20,
    #            color=dark_pink)

    #pyplot.text(-0.75, 0.78, r'BAO+$H_0$', fontsize=20,
    #            color=dark_orange)

    # pyplot.text(-0.4, 0.42, r'BAO', fontsize=30,
    #        color=dark_green)



    full_name = _build_figname(suffix=origin['FIGURE_NAME'])
    filename = "{0}-{1}.pdf".format(
        full_name,
        datetime.now().strftime('%Y-%m-%d')
    )
    # pyplot.savefig(filename, dpi=300, bbox_inches='tight')
    # pyplot.savefig("w0-zT-BAO.pdf",dpi=300, bbox_inches='tight' )
    print(">> Figure", filename, "saved")
    print()


exclude = (None,)

for idx, origin in enumerate(data_origins):
    if idx in exclude:
        continue
    make_contour_plot(origin)

pyplot.show()
