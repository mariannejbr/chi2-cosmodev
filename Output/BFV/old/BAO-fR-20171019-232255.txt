# 
# BAO_data =  6dF, DR7, DR13-1, DR13-3, SDSS(R),
# LyA1 &  LyA2 no WiggleZ and replacing LOWZ and
# CMASS by the latest results from BOSS-DR13
# 
# Best fit values for (chi2BAO min, w0, w1, w2, w3), 
# the corresponding likelihood max.
# Also calculates the GoF, BIC and AIC criteria.
# 
# bounds:
# w0, w1  w2, w3 = 
# 
# [(-0.8, 0.8), (0.1, 10), (0, 2 * pi), (1, 10)]
# 
# with polish
# 
# OmegaM = Planck
# 
# Results are displayed in the following order: chi2min, chi2reduced,
# w_params, h, OmegaM, likelihood max, GoF,
# BIC, AIC, and m (the number of free params)
# 
# 
9.747160206766075419e+00
3.249053402255358325e+00
2.087919623631014909e-01
1.000000000000000056e-01
5.101907486499234423e+00
6.726999999999999647e-01
3.139051079333047278e-01
2.931351793263776953e-24
1.104861172956415061e+00
1.161567977008904364e+02
1.163731571046691897e+02
4.000000000000000000e+00
