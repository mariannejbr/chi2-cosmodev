# 
#  w(z) = -1 - w0*exp(-z)*(z**w1 - z - w2)
# 
#     One parametrization to fit them all!
# 
# BAO data = 6dF, DR7, DR13-1, DR13-3, SDSS(R),
# LyA1 &  LyA2 no WiggleZ and replacing LOWZ and
# CMASS by the latest results from BOSS-DR13
# 
# H(z) data from cc measurements
# 
# SNe data from UNION 2.1
# 
# bounds 
# = [(-5, 5), (0, 10), (-2, 2),  (0.1, 1), (0.1, 1)]
# 
# Results are displayed in the following order: 
# w(z=0), chi2min, chi2reduced,
# w_params, h, OmegaM,
# aic, bic, m_params
# 
-8.716363275621481499e-01
5.722659640620061055e+02
9.748994277035879508e-01
5.000000000000000000e+00
1.124747389311322232e+00
-2.567273448757037210e-02
6.969169413317007900e-01
2.802549939842912674e-01
5.822659640620061055e+02
6.041834972364260921e+02
5.000000000000000000e+00
