# 
# BAO_data = 6dF, DR7, DR13-1, DR13-3, SDSS(R),
# LyA1 &  LyA2 no WiggleZ and replacing LOWZ and
# CMASS by the latest results from BOSS-DR13
# 
# H(z) data from cc measurements
# 
# SNe data from binned JLA
# 
# bounds for w_params
# w0, w1  w2, w3, OmM, h =
# 
# bounds = [(0, 1), (0, 10), (0, pi/2), (0, 10), (0.1, 1), (0.5, 1)]
# 
# Results are displayed in the following order: 
# chi2, chi2red, w0, w1, w2, w3,  hvar, OmegaM, 
# gof, aic, bic, m (number of free params)
# 
6.029202532386130287e+01
1.004867088731021818e+00
0.000000000000000000e+00
1.372129780237854257e+00
1.171540522147055619e+00
3.684163187441449328e+00
7.017859046972103609e-01
2.821120450519488676e-01
6.049264849606614712e-32
4.813656802812506044e+02
4.945036087334091803e+02
6.000000000000000000e+00
