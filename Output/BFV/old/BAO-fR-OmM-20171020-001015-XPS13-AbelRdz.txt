# 
# BAO_data =  6dF, DR7, DR13-1, DR13-3, SDSS(R),
# LyA1 &  LyA2 no WiggleZ and replacing LOWZ and
# CMASS by the latest results from BOSS-DR13
# 
# Best fit values for (chi2BAO min, w0, w1, w2, w3), 
# the corresponding likelihood max.
# Also calculates the GoF, BIC and AIC criteria.
# 
# bounds:
# w0, w1  w2, w3 = 
# 
# [(-0.8, 0.8), (0, 10), (0, 2 pi), (1, 10)]
# 
# with polish
# 
# OmegaM = OmegaM0
# 
# Results are displayed in the following order: chi2min, chi2reduced,
# w_params, h, OmegaM, likelihood max, GoF,
# BIC, AIC, and m (the number of free params)
# 
# 
1.580346769514195771e+01
5.267822565047318939e+00
-1.140490332320914096e-01
1.000000000000000000e+01
6.283185307179586232e+00
6.726999999999999647e-01
2.500000000000000000e-01
1.418918594518978126e-25
1.126975249998742878e+00
1.222131051892663152e+02
1.224294645930450685e+02
4.000000000000000000e+00
