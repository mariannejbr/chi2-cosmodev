# 
# BAO_data = newest BOSS  data
# Best fit values for (chi2BAO min, w0, w1, w2, h
# OmegaM), the corresponding likelihood max.
# Also calculates the GoF, BIC and AIC criteria.
# 
# bounds:
# w0, w1  w2, h, OmegaM = 
# 
# [(-0.5, 0.5), (-1, 1), (0, 2 pi), (0.5, 1), (0.1, 1)]
# 
# no polish
# 
# Results are displayed in the following order: chi2min, chi2reduced,
# w0, wi, q, zt, h, OmegaM, likelihood max, GoF,
# BIC, AIC, and m (the number of free params)
# 
# 
9.688440039660115133e+00
4.844220019830057566e+00
1.797639137837082346e-01
-1.302232049237630014e-09
4.875274846489427993e+00
7.048482061331636794e-01
4.672248170609025752e-01
3.018692416856735662e-24
9.921262435655839873e-01
1.180439876828397985e+02
1.183144369375632294e+02
5.000000000000000000e+00
