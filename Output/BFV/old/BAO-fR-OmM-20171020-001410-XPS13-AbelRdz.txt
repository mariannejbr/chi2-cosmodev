# 
# BAO_data =  6dF, DR7, DR13-1, DR13-3, SDSS(R),
# LyA1 &  LyA2 no WiggleZ and replacing LOWZ and
# CMASS by the latest results from BOSS-DR13
# 
# Best fit values for (chi2BAO min, w0, w1, w2, w3), 
# the corresponding likelihood max.
# Also calculates the GoF, BIC and AIC criteria.
# 
# bounds:
# w0, w1  w2, w3 = 
# 
# [(-0.8, 0.8), (0, 10), (0, 2 pi), (1, 10)]
# 
# with polish
# 
# OmegaM = OmegaM0
# 
# Results are displayed in the following order: chi2min, chi2reduced,
# w_params, h, OmegaM, likelihood max, GoF,
# BIC, AIC, and m (the number of free params)
# 
# 
9.951009961418940009e+00
3.317003320472979855e+00
7.332834558031184891e-01
1.000000000000000000e+01
6.283185307179586232e+00
6.726999999999999647e-01
4.000000000000000222e-01
2.647296160150312541e-24
1.106954415129074443e+00
1.163606474555433010e+02
1.165770068593220543e+02
4.000000000000000000e+00
