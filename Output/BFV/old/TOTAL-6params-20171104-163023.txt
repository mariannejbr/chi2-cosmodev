# 
# w(z) =  -1 + (w0 / (1 + (w1) * z ** (w3))) * np.cos(z + w2)
# 
# BAO_data = 6dF, DR7, DR13-1, DR13-3, SDSS(R),
# LyA1 &  LyA2 no WiggleZ and replacing LOWZ and
# CMASS by the latest results from BOSS-DR13
# 
# H(z) data from cc measurements
# 
# SNe data from binned JLA
# 
# bounds for w_params
# w0, w1  w2, w3, OmM, h =
# 
# bounds = [(-1.5, 1.5), (0, 10), (0, pi), (0, 10), (0.1, 1), (0.1, 1)]
# 
# Results are displayed in the following order: 
# w(z=0), chi2, chi2red, w0, w1, w2, w3,  hvar, OmegaM,
# gof, aic, bic, m (number of free params)
# 
-1.139043798321735856e+00
6.013775801813572741e+01
1.002295966968928864e+00
-7.910494905223952600e-01
6.506092273984803498e+00
1.394107113790149066e+00
4.755161123235633891e-01
7.041946129361585927e-01
2.776192274577592989e-01
