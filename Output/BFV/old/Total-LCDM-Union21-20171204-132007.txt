# 
# LCDM
# 
# w(z) =  -1 + ## (w0 / (1 + (w1) * z ** (w3))) * np.cos(z + w2)
# 
# BAO data = 6dF, DR7, DR13-1, DR13-3, SDSS(R),
# LyA1 &  LyA2 no WiggleZ and replacing LOWZ and
# CMASS by the latest results from BOSS-DR13
# 
# H(z) data from cc measurements
# 
# SNe data from UNION 2.1
# 
# bounds = [(0.1, 1), (0.1, 1)]
# 
# Results are displayed in the following order: 
# w(z=0), chi2min, chi2reduced,
# w_params, h, OmegaM,
# aic, bic, m_params
# 
-1.000000000000000000e+00
5.727938566958177944e+02
9.708370452471487555e-01
0.000000000000000000e+00
0.000000000000000000e+00
0.000000000000000000e+00
0.000000000000000000e+00
6.984532265497463976e-01
2.789542309952109944e-01
5.767938566958177944e+02
5.855608699655857663e+02
2.000000000000000000e+00
