# 
#          w(z) = -1 - A*exp(-z)*(z**n- z - C)
#             * * * New lims for params * * *
#         
#         QUINTESSENCE-LIKE : n =0 
#         
#         A in [0,50] and C in [-5,0]
#           
#         Results are displayed in the following order: 
#         w(z=0), chi2min, chi2reduced,
#         w_params, h, omegabh2, omegach2, OmegaM,
#         aic, bic, m_params
# 
#         bounds = [(A),       (n),    (C),     (h), (omegabh2_fixed), (omegach2)]
#         bounds = [(0, 50),   (1), (-5, 0), (0.5, 0.8), (0.005, 0.045), (0.001, 0.2)]
# 
#         
-1.362264384594689792e+00
1.598547225041914999e+01
6.660613437674646198e-01
3.613415721159825011e-01
0.000000000000000000e+00
-2.553850843409311011e-03
7.264261328389729488e-01
2.222000000000000017e-02
1.140345120009025603e-01
2.582069774898453351e-01
2.398547225041914999e+01
2.931429029111996698e+01
4.000000000000000000e+00
