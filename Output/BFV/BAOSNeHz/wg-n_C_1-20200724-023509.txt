# 
#          w(z) = -1 - A*exp(-z)*(z**n- z - C)
#             * * * New lims for params * * *
#         
#         EXPONENTIAL FORM LIKE : n = C = 1
#           
#         Results are displayed in the following order: 
#         w(z=0), chi2min, chi2reduced,
#         w_params, h, omegabh2, omegach2, OmegaM,
#         aic, bic, m_params
# 
#         bounds = [(A),       (n),    (C),     (h), (omegabh2_fixed), (omegach2)]
#         bounds = [(-50, 50), (0, 1), (-5, 5), (0.5, 0.8), (0.005, 0.045), (0.001, 0.2)]
# 
#         
-1.003582419228270739e+00
5.729638224172335867e+02
9.727738920496326047e-01
-3.582419228270738998e-03
1.000000000000000000e+00
1.000000000000000000e+00
6.991942124402796832e-01
2.222000000000000017e-02
1.126149261698035503e-01
2.758079321025824759e-01
5.789638224172335867e+02
5.921143423218856015e+02
3.000000000000000000e+00
