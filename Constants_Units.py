# # # Mariana Jaber  2017(2018)(2019)
#     This part of the program contains the basic cosmological constants
#     It is based on Planck 2015 Cosmological Parameters report
#     (arXiv:1502.01589) table 3 column 1:
#     "Planck + TT + lowP".
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # MAIN UPDATE 12.xii.19: BASE COSMOLOGY HAS BEEN CHANGED  from
# # # Planck TT + TE + EE + lowP to
# # # Planck TT + lowP
# -----------------------------------------
# # # 1502.01589 (Cosmological parameters)
# # # 1502.01590 (DE & MG)
# -----------------------------------------

import numpy as np

PI = np.pi
C = 299792458.
HBAR = 6.62606896 / (2 * PI) * 1e-34
E = 1.6021773 * 1e-19  # electron unit charge
G = 6.6738 * 1e-11  # Newton's gravity constant
Mpc = 3.085678 * 1e22  # Mpc-meters
KBoltz = 1.3806504 * 1e-23

# Masa  de Planck y masa reducida de Planck en eV
MPlanck = np.sqrt(HBAR * C / (8 * PI * G)) * C ** 2 / E
mplanck = MPlanck * np.sqrt(8 * PI * G)

# temperatura del CMB (en Kelvin), neutrinos
Tg0 = 2.7255
Neff = 3.13  # 3.046 vanilla  # 3.13 (eqn. 60a-60d, arXiv:1502.01589)
THETHADEC = 1.04105e-2
TAU = 0.079
ZDRAG = 1059.57
ZDEC = 1090.09

# Hubble parameter:
REDUCED_H0 = 0.6731
H0 = (REDUCED_H0 * 1e5) / C
# constante que aparece en los calculos
H0p = (100 * 1e3) / 2.99792458e+8

# critical density
RHOCR0 = 3 * (REDUCED_H0 * MPlanck * E * Mpc * 10 ** 5 / (C ** 2 * HBAR)) ** 2

# today's value of radiation density
RHOR0 = (1 + Neff * 7. / 8. * (4. / 11.) ** (
        4. / 3.)) * PI ** 2 / 15. * Tg0 ** 4 * (KBoltz * Mpc / (
        C * HBAR)) ** 4

# ------------------------------
# The default value of the physical densities

# physical densities for different species: baryon, rad, photons,cdm and DE component
OMEGACH2 = 0.1197
OMEGABH2 = 0.02222

OMEGAR0 = RHOR0 / RHOCR0
OMEGAG0 = 4.64512e-31 / 8.53823e-27

OMEGAB0 = OMEGABH2 / (REDUCED_H0 ** 2)
OMEGADE = 0.685
OMEGADEH2 = (REDUCED_H0 ** 2) * OMEGADE
# fractional matter density
OMEGAM0 = OMEGAB0 + OMEGACH2 / (REDUCED_H0 ** 2)

# OMEGAK0 = 0

RhoM0 = OMEGAM0 * RHOCR0
RhoDE0 = OMEGADE * RHOCR0

# RHOCR0 = (3 * 1.48e-42 ** 2) / (8 * pi * 6.707e-39)

# --------------------------------------------------------------------#
# --------------------------------------------------------------------#
# ========= Planck 2015 DE & MG paper Sect'n 5.1.6
# DMATCMB: is the normalized matrix for R, l_A and omegab (with/without ns)
# Table 4 from Planck 2015 DE&MG paper
# We're using the values assuming Planck TT + lowP and marginalizing over A_L

# 3x3 matrix
DMATCMB_Al_3 = np.array(
    [[1.0, 0.64, -0.75], [0.64, 1.0, -0.55], [-0.75, -0.55, 1.0]])
ERRCMB_Al_3 = np.array([0.0088, 0.15, 0.00029])

# Update feb 12 2020: add the full matrix / to extend it to include ns
# 4x4 matrix
DMATCMB_Al_4 = np.array(
   [[1.0, 0.64, -0.75, -0.89], [0.64, 1.0, -0.55, -0.57],
   [-0.75, -0.55, 1.0, 0.71], [-0.89, -0.57, 0.71, 1.0]])
ERRCMB_Al_4 = np.array([0.0088, 0.15, 0.00029, 0.0072])


COVMATCMB_3 = DMATCMB_Al_3 * ERRCMB_Al_3 * ERRCMB_Al_3[:, np.newaxis]
COVMATCMB_4 = DMATCMB_Al_4 * ERRCMB_Al_4 * ERRCMB_Al_4[:, np.newaxis]

# --------------------------------------------------------------------#
# base_TTTEEE_lowl_plik.covmat from COSMOMC 2015
oldCOVMATCMB = np.array([[23.52482, -2.207815], [-2.207815, 1.063561]]) * 1e-7
oldCOVMATCMB10e7 = np.array([[23.52482, -2.207815], [-2.207815, 1.063561]])
oldINVCOVMATCMB = np.linalg.inv(oldCOVMATCMB)
oldINVCOVMATCMB10e7 = np.linalg.inv(oldCOVMATCMB10e7)
#COVMATCMB_wrong = ERRCMB_Al * ERRCMB_Al * DMATCMB_Al
