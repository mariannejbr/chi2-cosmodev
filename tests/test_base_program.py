# ====================   Start some tests ========================
# # test only in terminal

import os
import sys

file_path = os.path.dirname(os.path.abspath(__file__))
print(file_path)
sys.path.insert(0, os.path.join(file_path, '..'))
from Cosmology_DE import *

# LCDM_PARAMS = (0, 0, 0, 1), (REDUCED_H0, OMEGAM0)
w_params_lcdm = (0, 0, 0)  # depends on the form of w(z)
cosmo_params_lcdm = (REDUCED_H0, OMEGABH2, OMEGACH2)

# EXTREME_PARAMSh0 = (0, 1, 10, 3, REDUCED_H0, 0.15)


# def test_wz():
#    w0, wi = -1, -1
#    z_test = 1000
#    assert wz(z_test, w0, wi) == -1
# def test_H0p():
#     assert abs(H0p - 1)<1e-10
#
# def test_Hubble():
#     """tests for special cases of Hubble."""
#     # assert abs(hubbleflat(0, *H0chi2) - H0) < 1e-20
#     # assert abs(hubbleflat(0, *LCDM_PARAMS) - H0) < 1e-20
#     # assert abs(hubbleflat(0, *LCDM_PARAMS) - hubbleflat(0, *H0chi2)) < 1e-20
#     assert abs(hubbleflat(0,*EXTREME_PARAMSh0) - H0p * REDUCED_H0) < 1e-20


# def test_chi2H0():
#     assert np.abs(chi2H0(*LCDM_PARAMS) - chi2H0(*EXTREME_PARAMSh0)) < 1e-20
#
# def test_chi2CMB():
#     assert np.abs(chi2CMB(*LCDM_PARAMS2) - 1) < 1e-10
# def test_chiH0():
#     assert abs(hubbleflat(0,*H0chi2)-H0)< 1e-10
# print(hubbleflat(0, *H0chi2))

# def test_f_DEz():
#     """
#
#     :return:
#     """
#     v1 = f_DEz_cc(0.5, w_params_lcdm)
#     v2 = f_DEz(0.5, w_params_lcdm)
#     assert abs(v1 - v2) <= 1e-8

# def test_dlum():
#    assert np.abs(distance_SNe(0.5, w_params_lcdm, cosmo_params_lcdm) - 100) < 1


# def test_mu():
#    assert np.abs(mu_SNe(0.5, w_params_lcdm, cosmo_params_lcdm) - 42) < 1e-3


# def test_wx():
#     wparams = 10, 0, 0
#     assert np.abs(wz(0, wparams) - 1) < 1e-3
cosmoparams_failed = 0.36155281, 0.08867716, 0.15067219
wparams_failed = -0.94780419, 0, 5.88677253


def test_hubbleflat():
    assert np.abs(
        hubbleflat(0.02, wparams_failed, cosmoparams_failed) - 100) < 1


def test_rs():
    assert np.abs(r_s(ZDEC, w_params_lcdm, cosmo_params_lcdm) - 150) < 1


def test_Rcmb():
    assert np.abs(Rcmb(w_params_lcdm, cosmo_params_lcdm) - 1.7) < 0.001


def test_lA():
    assert np.abs(l_A(w_params_lcdm, cosmo_params_lcdm) - 1) < 0.1


# def test_chi2sne():
#     assert np.abs(chi2SNe(*LCDM_PARAMS) - 1)<1e-3

def test_hflat():
    assert np.abs(hubbleflat(0.01, wparams_failed, cosmoparams_failed) - 10) < 1


wparams_failed2 = 7.99237291, 1.02733782, 0.89331134
cosmoparams_failed2 = 0.54463432, 0.05622952, 0.501585


def test_dang_int():
    assert np.abs(
        d_ang_integrand(0.01, wparams_failed2, cosmoparams_failed2) - 10) < 1


#def test_chi2_select():
#    assert np.abs()

# def test_chi2bao():
#     assert np.abs(chi2BAO(*LCDM_PARAMS)-10)<1e-3
#
#
# def test_chi2baosne():
#     assert np.abs(chi2BAOSNe(*LCDM_PARAMS) - 10) < 1e-3

# def _test_chi2BAO_parallel():
#     """"""
#
#     # The data will consist of 20 rows with a given value of w0 and the
#     # same value of wi, q and zt.
#
#     # Ten data points
#     w0_values = 10
#     test_data = np.zeros((w0_values, 4))
#
#     # Let's fix the values of wi, q and zt.
#     test_data[:, (1, 2, 3)] = (-1, 1, 1)
#
#     # Now let's put some values to w0
#     w0_range = np.linspace(-1, -0.99, w0_values)
#     test_data[:, 0] = w0_range[:]
#
#     chi2BAO_data = parallel_chi2BAOuncorr(test_data)
#
#     assert chi2BAO_data.shape == (w0_values, 2)
#
#     print(chi2BAO_data)
#     assert False

# if __name__ == '__main__':
# print(__name__)
# print(F_DEz(0, -10, -91, 1, 1))
# print(hubbleflat(0, -1, -1, 1, 1))
# print(Hubble(z=1, w0=-1, zt=1, wi=-1, q=1, omegach2_local=0))
#     print(hubbleflat(0,-1,-1,1,1,REDUCED_H0,OMEGAM0) - H0)
# print(Hubble(zdrag))
# print(wz(0))
# print(F_DEz(10))
# print(r_s(zdrag))
# print(D_vz(0.57))
# print(rBAO(zdrag, -1, -1, 1, 1))
# print(omegar0, omegam0(),omegade0(), H0)
