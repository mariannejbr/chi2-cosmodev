# ====================   Start some tests ========================
# # test only in terminal

import sys, os
import numpy as np

file_path = os.path.dirname(os.path.abspath(__file__))
print(file_path)
sys.path.insert(0, os.path.join(file_path, '..'))

#from Cosmology_DE import wz
from wz_gus import *
from Constants_Units import REDUCED_H0, OMEGAM0

w_lcdm = 0, 1, 1

def test_lcdm():
    w_params = w_lcdm
    #cosmo_parms = OMEGAM0, REDUCED_H0
    assert wz(0, w_params) -1 < 1e-5

def test_wzgus():
    w_params = -47, 0.3, 0.01
    #cosmo_params = OMEGAM0, REDUCED_H0
    assert wz(0, w_params) == 0


