import os
import sys

file_path = os.path.dirname(os.path.abspath(__file__))
print(file_path)
sys.path.insert(0, os.path.join(file_path, '..'))
from Statistics.chi2_likelihood import *

cosmoparams_failed = 0.36155281, 0.08867716, 0.15067219
wparams_failed = -0.94780419, 0, 5.88677253

wparams_failed2 = 7.99237291, 1.02733782, 0.89331134
cosmoparams_failed2 = 0.54463432, 0.05622952, 0.501585

wparams_lcdm = 0, 1, 0
cosmoparams_lcdm = REDUCED_H0, OMEGABH2, OMEGACH2
def test_chi_h0():
    assert np.abs(chi2hz(wparams_failed, cosmoparams_failed) - 1) < 0.1


def test_chi2_CMB():
    assert np.abs(chi2CMB(wparams_lcdm, cosmoparams_lcdm) - 1) < 0.1

def test_chi2_total():
    wparams = -4.7, 0.5, 0.2
    assert np. abs(chi2BAOCMB(wparams_lcdm, wparams) - 1) < 0.1

