# ====================   Start some tests ========================
# # test only in terminal
import sys, os
import numpy as np

file_path = os.path.dirname(os.path.abspath(__file__))
print(file_path)
sys.path.insert(0, os.path.join(file_path, '..'))

from Cosmology_DE import *

LCDM_PARAMS = (-1, -1, 1, 1, REDUCED_H0, OMEGACH2)
# LCDMbis_PARAMS = (-1, -1, 1, 1, 0.5, OMEGACH2)
# VANILLA1_PARAMS = (-0.9, -1.4, 9, 0.3, REDUCED_H0, 0.1180)
# VANILLA2_PARAMS = (-0.9, -1.4, 9, 0.3, 0.4, 0.1180)
# EXTREME_PARAMS = (0, 1, 10, 3, 0.4, 0.15)
MODELA = (-0.92, -0.99, 9.97, 0.28, 0.7322, 0.1568)
MODELB = (-0.92, -0.77, 9.8, 0.63, 0.678, 0.1195)
MODELC = (-0.96, 0, 1.5, 1.31, 0.7326, 0.1195)

LCDM_A = (-1, -1, 1, 1, 0.7356, 0.1476)
LCDM_B = (-1, -1, 1, 1, 0.7020, 0.1201)
LCDM_C = (-1, -1, 1, 1, 0.7099, 0.1203)


# datafile1 = os.path.join(file_path, '../exec/data/BAO/')
# [chi2min1, hval_test1, omch2best1, maxlikebao, gof, bic, aic,
#  mparams] = np.loadtxt(datafile1)

# datafile2 = os.path.join(file_path, '../exec/data/BFV-7_LCDM_hfixed-TEST.txt')
# [chi2min2, hval_test2, omch2best2, maxlikebao, gof, bic, aic,
#  mparams] = np.loadtxt(datafile2)
#
# datafile3 = os.path.join(file_path, '../exec/data/BFV-7_LCDM_hvar-TEST.txt')
# [chi2min3, hval_test3, omch2best3, maxlikebao, gof, bic, aic,
#  mparams] = np.loadtxt(datafile3)


# LCDM1 = (-1, -1, 1, 1, hval_test1, omch2best1)
# LCDM2 = (-1, -1, 1, 1, hval_test2, omch2best2)
# LCDM3 = (-1, -1, 1, 1, hval_test3, omch2best3)

# def test_constants():
#     assert not (
#         REDUCED_H0, H0, OMEGAR0, OMEGAM0, OMEGADE, OMEGAG0
#     )
#
# def test_wz():
#    w0, wi = -1.67, -1
#    z_test = 1000
#    assert wz(z_test, w0, wi) == -1
#
#
# def test_f_DEz():
#     assert not f_DEz(100, -1.67, -1, 1, 1)
#
#
# def test_hubble():
#    """tests for special cases of Hubble."""
#    assert not hubbleFunc(100, -1, -1, 1, 1, OmegaM=0.3156)

#
# def test_rs():
#     assert not r_s(ZDEC, -1, -1, 1, 1, OmegaM=0.3156)

def test_theta_dec():
    # assert  theta_dec(*MODELA) == theta_dec(*LCDM_A)
    # assert theta_dec(*MODELB) == theta_dec(*LCDM_B)
    assert theta_dec(*MODELC) == theta_dec(*LCDM_C)


# def test_rBAO():
#    assert not rBAO(0.57, -1, -1, 1, 1, OmegaM=0.3156)

def test_rs_dec():
    OmM_C = (OMEGABH2 + 0.1195) / (0.7326 ** 2)
    MODELCb = (-0.96, 0, 1.5, 1.31, 0.7326, OmM_C)

    OmM_CLa = (OMEGABH2 + 0.1203 ) / (0.7099)**2
    LCDM_C_b= (-1, -1, 1, 1, 0.7099, OmM_CLa)
    assert r_s(ZDEC, *MODELCb) == r_s(ZDEC, *LCDM_C_b)


def test_da_dec():
    assert thetadec_integrand(ZDEC, *MODELC) == thetadec_integrand(ZDEC, *LCDM_C)


def test_chi2bao():
    OmM_C = (OMEGABH2 + 0.1195) / (0.7326 ** 2)
    MODELCb = (-0.96, 0, 1.5, 1.31, 0.7326, OmM_C)
    OmM_B = (OMEGABH2 + 0.1195) / (0.678 **2)
    MODELBb = (-0.92, -0.77, 9.8, 0.63, 0.678, OmM_B)
    assert chi2BAO(*MODELCb) == chi2BAO(*MODELBb)

# def test_chi2BAOuncorr():
#     assert not chi2BAOuncorr(-1, -0.6, 1, 1, h=0.99, OmegaM=0.3156)



# def test_chi2BAO():
#     assert np.abs(chi2BAO(*LCDM1) - chi2min2) < 1e-10


# def test_likeBAOuncorr():
#     assert LikeBAOuncorr(*LCDM_PARAMS) == LikeBAOuncorr(*LCDMbis_PARAMS)

# def test_chi2CMB():
#     assert chi2CMB(*VANILLA1_PARAMS) ==chi2CMB(*VANILLA2_PARAMS)

# #
# #
# def test_Totalchi2():
#     assert Totalchi2(*VANILLA1_PARAMS) == Totalchi2(*VANILLA2_PARAMS)
# #
# def test_TotalLikelihoodBAO():
#     assert TotalLikelihood(*VANILLA2_PARAMS) == TotalLikelihood(*VANILLA1_PARAMS)


# def _test_chi2BAO_parallel():
#     """"""
#
#     # The data will consist of 20 rows with a given value of w0 and the
#     # same value of wi, q and zt.
#
#     # Ten data points
#     w0_values = 10
#     test_data = np.zeros((w0_values, 4))
#
#     # Let's fix the values of wi, q and zt.
#     test_data[:, (1, 2, 3)] = (-1, 1, 1)
#
#     # Now let's put some values to w0
#     w0_range = np.linspace(-1, -0.99, w0_values)
#     test_data[:, 0] = w0_range[:]
#
#     chi2BAO_data = parallel_chi2BAOuncorr(test_data)
#
#     assert chi2BAO_data.shape == (w0_values, 2)
#
#     print(chi2BAO_data)
#     assert False

# if __name__ == '__main__':
# print(__name__)
# print(F_DEz(0, -10, -91, 1, 1))
# print(Hubble(1, -1, -1, 1, 1))
# print(Hubble(z=1, w0=-1, zt=1, wi=-1, q=1, omegach2_local=0))
# print(Hubble(0) - H0)
# print(Hubble(zdrag))
# print(wz(0))
# print(F_DEz(10))
# print(r_s(zdrag))
# print(D_vz(0.57))
# print(rBAO(zdrag, -1, -1, 1, 1))
# print(omegar0, omegam0(),omegade0(), H0)
