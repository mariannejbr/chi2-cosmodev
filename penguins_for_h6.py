# -*- encoding: utf-8 -*-
import os
import os.path as path
import sys
from datetime import datetime

import matplotlib as mpl
import matplotlib.pyplot as pyplot
import numpy as np
import scipy.ndimage as ndimage
from h5py import File
from matplotlib.ticker import Formatter

current_dir = os.path.dirname(os.path.abspath(__file__))
print(current_dir)

BASE_PATH = path.join(current_dir, 'chi2_cosmo_dev', 'output')



mpl.rcParams['xtick.labelsize'] = 20
mpl.rcParams['ytick.labelsize'] = 20
mpl.rcParams['mathtext.fontset'] = 'cm'


TOTALGRIDS_PATH = path.join(BASE_PATH, 'grids')
TOTALBFV_PATH = path.join(BASE_PATH, 'bfv')

SNeGRIDS_PATH = path.join(BASE_PATH, 'grids')
SNeBFV_PATH = path.join(BASE_PATH, 'bfv')

HzGRIDS_PATH = path.join(BASE_PATH, 'grids')
HzBFV_PATH = path.join(BASE_PATH, 'bfv')


BASE_FIGURE_NAME = 'Contour'

data_origins = [
    # First figure, beta-Omega_m contour
#     {
#         'WqCDM': [
#             {
#                 'best_fit': np.loadtxt(path.join(
#
#                     TOTALBFV_PATH, '20190403-190211-chi2SNeU21-CC_beta_h_OmM.txt'
#
#                 )),
#                 'data_grid': File(path.join(
#
#                     TOTALGRIDS_PATH, 'beta-OmM_CC-SNeU21-grid-20190403-205000.h5'
#                     #TOTALGRIDS_PATH, 'h-OmM_CC-SNeU21-grid-20190403-204408.h5'
#
#                 )),
#
#                 'delta_chi2': np.array([3.53, 8.02, 14.2])
#                 # m = 3 parameters fitted
#
#             }
#             ,
# {
#                 'best_fit': np.loadtxt(path.join(
#
#                     HzBFV_PATH, '20190403-173034-chi2Hz_beta_h_OmM.txt'
#
#                 )),
#                 'data_grid': File(path.join(
#
#                     HzGRIDS_PATH, 'beta-OmM_CC-grid-20190403-191315.h5'
#
#                 )),
#
#                   'delta_chi2': np.array([3.53, 8.02, 14.2])
#                      # m = 3 parameters fitted
#
#             }
#           #  ,
#          #   {
#           #      'best_fit': np.loadtxt(path.join(
#
#            #         SNeBFV_PATH, '20190403-184022-chi2SNeU21_beta_h_OmM.txt'
#
#             #    )),
#              #   'data_grid': File(path.join(
#
#               #      beta-OmM_Union21-grid-20190404-020938.h5'
#                # )),
#
#                # 'delta_chi2': np.array([3.53, 8.02, 14.2])
#                 # m = 3 parameters fitted
#                 #
#         #    }
#
#         ],
#
#       #  'FIGURE_NAME': 'hOmM-CCSNeU21',
#         'FIGURE_NAME': 'betaOmM',
#         'LEGEND': r'',
#
#         'PLOT_LIMITS': [[0, 1e-2], [0.1, 0.4]]
#        # 'PLOT_LIMITS': [[0.6, 0.8], [0.1, 0.4]]
#     }
#      ,
    # #
    # Second figure, h-OmegaM contour
     {
         'WqCDM': [
{
                'best_fit': np.loadtxt(path.join(

                    HzBFV_PATH, '20190403-173034-chi2Hz_beta_h_OmM.txt'

                )),
                'data_grid': File(path.join(

                    HzGRIDS_PATH, 'h-OmM_CC-grid-20190404-012736.h5'

                )),

                'delta_chi2': np.array([3.53, 8.02, 14.2])
                # m = 3 parameters fitted

            }
             ,
             {
                 'best_fit': np.loadtxt(path.join(
                     SNeBFV_PATH, '20190403-184022-chi2SNeU21_beta_h_OmM.txt'
                 )),
                 'data_grid': File(path.join(
                     SNeGRIDS_PATH, 'h-OmM_Union21-grid-20190404-014601.h5'
                 )),

                 'delta_chi2': np.array([3.53, 8.02, 14.23])
                 # m = 3 parameters fitted
                 #
             },


             {
                 'best_fit': np.loadtxt(path.join(
                    TOTALBFV_PATH, '20190403-190211-chi2SNeU21-CC_beta_h_OmM.txt'
                )),
                'data_grid': File(path.join(
                    TOTALGRIDS_PATH, 'h-OmM_CC-SNeU21-grid-20190403-211018.h5'
                )),

                'delta_chi2': np.array([3.53, 8.02, 14.2])
                # m = 3 parameters fitted

            }


        ],

        'FIGURE_NAME': 'h-OmegaM',
        'LEGEND': r'',

        'PLOT_LIMITS': [[0.55, 0.9], [0, 0.5]]
    }

]



##########################################################

class FancyFormatter(Formatter):
    """Formater for values with LaTeX fonts."""

    def __call__(self, kS, pos=None):
        return r'${:.5G}$'.format(kS)

    def format_data_short(self, value):
        return '{:.1f}'.format(value)


def _extract(h5file):
    # return h5file['/DataGrid'], h5file['/Chi2BAO']
    return h5file['/DataGrid'], h5file['/Chi2']


def _build_figname(suffix=None):
    if suffix is None:
        return BASE_FIGURE_NAME
    return '{0}-{1}'.format(BASE_FIGURE_NAME, suffix)


def make_contour_plot(origin):
    dark_pink = '#800080'
    light_pink = '#FFB3FF'

    dark_blue = '#003366'
    light_blue = '#99CCFF'

    dark_orange = '#FF8000'
    light_orange = '#FFBF80'

    dark_green = '#208000'
    light_green = '#C6FFB3'

    dark_red = '#CC0000'
    light_red = '#DEA6A6'

    colors = [

        [dark_blue, light_blue],
        [dark_orange, light_orange],
        [dark_pink, light_pink],
        [dark_green, light_green],
        [dark_red, light_red]
    ]
    alphas = [
        [1, 0.5],
        #[0.9, 0.6],
        [0.8, 0.7],
        [0.75, 0.6]
    ]

    figsize = (6, 6)  # Width, height inches

    pyplot.figure(figsize=figsize)

    ###
    for idx, H6th_data_set in enumerate(origin['WqCDM'][:]):
        print(">> Data used for contouring")
        print(idx, H6th_data_set)

        # H6th_best_fit = _extract(H6th_data_set['best_fit'])
        H6th_best_fit = H6th_data_set['best_fit']
        H6th_grid_data, H6th_chi2_data = _extract(H6th_data_set['data_grid'])

        delta_chi2 = H6th_data_set['delta_chi2']
        chi2min = H6th_best_fit[0]


        BETA = H6th_grid_data[:, :, 0]
        H0 = H6th_grid_data[:, :, 1]
        OmM = H6th_grid_data[:, :, 2]


        print(BETA)
        print(H0)
        print(OmM)

        chi2 = H6th_chi2_data[:, :, 0]
        print(chi2)

        chi2 = ndimage.interpolation.spline_filter(chi2, order=3)

        levels = chi2min + delta_chi2
        print(">> WqCDM levels:", levels)
        levels[0] = 0
        print(levels)
        # FOR FIRST FIGURE
       # pyplot.contour(BETA, OmM, chi2, levels=levels,
         #              colors=colors[idx][0],  # colorscolors
         #              linewidths=1, alpha=alphas[idx][0])

       # cs = pyplot.contourf(BETA, OmM, chi2, levels=levels,
         #                    colors=colors[idx], alpha=alphas[idx][1])

        # # FOR SECOND FIGURE
        pyplot.contour(H0, OmM, chi2, levels=levels,
                       colors=colors[idx][0],  # colorscolors
                       linewidths=1, alpha=alphas[idx][0])

        cs = pyplot.contourf(H0, OmM, chi2, levels=levels,
                             colors=colors[idx], alpha=alphas[idx][1])#

    axes = pyplot.gca()
    axes.xaxis.set_major_formatter(FancyFormatter())
    axes.yaxis.set_major_formatter(FancyFormatter())
    # cbar = pyplot.colorbar(cs, label=r"$\chi^2$")
    # print(best_fit)
    # print(best_fit.reshape([2, 1]))

    pyplot.xlim(origin['PLOT_LIMITS'][0])
    pyplot.ylim(origin['PLOT_LIMITS'][1])

    # pyplot.legend(cs)
    pyplot.xlabel(r'$h$', fontsize=28)
    #pyplot.ylabel(r'$\beta$', fontsize=28)
    pyplot.ylabel(r'$\Omega_M$', fontsize=28)



    ## adapt coordinates to each figure
    pyplot.text(0.60, 0.2, r'CC ', fontsize=20,
                color=dark_blue)

    pyplot.text(0.60, 0.15, r'SNeIa', fontsize=20,
                color=dark_orange)

    pyplot.text(0.60, 0.1, r'CC+SNeIa', fontsize=20,
               color=dark_pink)

    # pyplot.text(-0.4, 0.42, r'BAO', fontsize=30,
    #        color=dark_green)



    full_name = _build_figname(suffix=origin['FIGURE_NAME'])
    filename = "{0}-{1}.pdf".format(
        full_name,
        datetime.now().strftime('%Y-%m-%d')
    )
    pyplot.savefig(filename, dpi=300, bbox_inches='tight')
    #pyplot.savefig("H-omegaM.pdf",dpi=300, bbox_inches='tight' )
    #print(">> Figure", filename, "saved")
    print()


exclude = (None,)

for idx, origin in enumerate(data_origins):
    if idx in exclude:
        continue
    make_contour_plot(origin)

pyplot.show()
